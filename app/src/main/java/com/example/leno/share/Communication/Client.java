package com.example.leno.share.Communication;

/**
 *
 * Internal imports
 *
 * */


import com.example.leno.share.ErrorManager.ErrorManager_generic;
import com.example.leno.share.FileManager.ANDROID.FileManager_android;
import com.example.leno.share.FileManager.FileManager;
import com.example.leno.share.FileManager.PC.FileManager_pc;
import com.example.leno.share.R;
import com.example.leno.share.ThreadManager.Flag_Carrier;
import com.example.leno.share.ThreadManager.ThreadManager_dataHolder;
import com.example.leno.share.ThreadManager.ThreadManager_generic;
import com.example.leno.share.UI_Manager.Dialogs.Dialog_connection;
import com.example.leno.share.UI_Manager.UI_handler.UI_main;


/**
 *
 * Android imports
 *
 * */
import android.app.Activity;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;


/**
 *
 *Java imports
 * */
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Collections;
import java.util.concurrent.locks.ReentrantLock;


/**
 * Created by LeNo on 5.9.2017..
 *
 *
 * Purpose of class:
 *              -main class for downloading/uploading files and establishing connection with server (PC)
 *
 *
 *              Detailed description->
 *                      First section->
 *                              SearchingPC->
 *                                      "searchServer_Init" function is a entry point for "searching" PC,
 *                                      in "getSubnetAddress" function extract wi-fi ip address,
 *                                      and removing the last number,it should look like this (192.168.5.).
 *                                      After that we simple loop 256 time and each time we put another number
 *                                      to last place in address (192.168.5.0,192.168.5.1.......).We loop until
 *                                      we find the ip address of pc that we want to connect (PC must have installed
 *                                      application)
 *
 *                                      Note:
 *                                          if address is "0.0.0." that means we are not connected to wi-fi.
 *
 *                              ConnectingPC->
 *                                  -> using password and ipaddress from "Edit texts" in "Dialog_connection" class,
 *                                  we are sending password to ipaddress and waiting for response.If response is 1,then
 *                                  we are connected to PC and we can "start app",else "Thread" is finished and available
 *                                  for another try.
 *
 *                    Second section->
 *                              functions for handling requests and response for or from server.
 */



/**
 * First section->
 *          Searching for PC address->
 *                  Important note on functions->
 *                                  parseCoreAddress(){
 *
 *                                      Loop from the address size -2 (192.168.5.1-Putting index here 1)
 *                                      and then moving index to 0 and checking where is the first dot (.).
 *                                      When first dot is found we return part of string (192.168.5.11->192.168.5.)
 *                                  }
 *                                  searchServer(){
 *
 *
 *                                      this function checks all combinations of ip addressees(from 0 to 256).
 *                                      The way it checks is with loop in range from 0 to 256.In loop it will check
 *                                      if there is available thread (max.number of active threads is 2,
 *                                      line "if (i%2==0)"flgCarr.T_checkAddr_nAvailable()" takes care that there are only two active threads;).
 *
 *                                      In function "checkAddress" we call "flgCarr.T_checkAddr_Available()" that will set free thread after
 *                                      "checking" is done.
 *
 *                                  }
 *
 *
 *
 *
 * Second section
 *             -> sendRequest_init function
 *                                      -> uses "switch" loop for request ident. and calling appropriate function to handle the request.
 *
 *             -> (Request for "root" files) loadRoot function
 *                                              -> sends "ROOT*" string to server and waits for response,that will be displayed.
 *
 *
 *
 *
 *
 * */

public class Client implements Serializable {

    private final ReentrantLock Locker=new ReentrantLock();

    private  ErrorManager_generic errMng;
    private  Flag_Carrier flgCarr;
    private  UI_main UIhnd;
    private  ThreadManager_dataHolder dtHld;
    private  ByteManager byMng;

    private Socket client;
    private BufferedInputStream client_inStream;
    private BufferedOutputStream client_outStream;

    private String passwordOfPC;
    public String ipAddressOfPC;

    /**
     *
     *
     * FIRST SECTION----------------------------SEARCH PC----------------------------------------------------------------------------------
     *
     * */

    public void searchServer_Init(Flag_Carrier flgCr,
                                  ThreadManager_dataHolder dataHld,
                                  UI_main UI_h,
                                  ErrorManager_generic err,
                                  Activity activity,
                                  ThreadManager_generic thrMng,
                                  Dialog_connection dialogConn)
    {
        errMng=err;
        flgCarr=flgCr;
        UIhnd =UI_h;
        dtHld=dataHld;


        String ip_address=getSubnetAddress(activity);

        if (!ip_address.contains("0.0.0"))
        {
            searchServer(ip_address,thrMng,dialogConn);

            flgCarr.T_connectPC_Deactivate();
        }
        else
        {
            flgCarr.T_connectPC_Deactivate();
        }

    }

    private String getSubnetAddress(Activity activity){

        WifiManager wifiMan = (WifiManager) activity.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInf = wifiMan.getConnectionInfo();

        int ipAddress = wifiInf.getIpAddress();

        return parseCoreAddress
                (String.format("%d.%d.%d.%d", (ipAddress & 0xff),(ipAddress >> 8 & 0xff),(ipAddress >> 16 & 0xff),(ipAddress >> 24 & 0xff)));
    }

    private String parseCoreAddress(String address){

        for (int i=address.length()-2; i>0; i--)
        {
            if (address.charAt(i)=='.')return address.substring(0,i+1);
        }
        return address;
    }


    private void searchServer(String coreAddress,ThreadManager_generic thrM,Dialog_connection dialogConn)
    {

        for (int i = 0; i < 256; i++) {

            if (flgCarr.S_ifConnected()) break;

            else if(thrM.thread_Init(this,"checkAddress",coreAddress.concat(Integer.toString(i)),dialogConn,String.class,Dialog_connection.class,1))
            {
                if (i%4==0)flgCarr.T_checkAddr_nAvailable();
            }
            else
            {
                --i;
            }

        }
        flgCarr.T_checkAddr_Available();
        dtHld.sendRequest(6390);
        }



    public void checkAddress(String ipAddress,Dialog_connection dialogConn){
        try
        {
            Socket tempclient=new Socket();
            tempclient.connect(new InetSocketAddress(ipAddress,3000),50);

            BufferedInputStream tempclient_inStream=new BufferedInputStream(tempclient.getInputStream());
            BufferedOutputStream tempclient_outStream=new BufferedOutputStream(tempclient.getOutputStream());

            tempclient_outStream.write("APPLICATION STARTED*".getBytes());
            tempclient_outStream.flush();

            checkResponse_init(tempclient_inStream,tempclient_outStream,ipAddress);

            client=tempclient;
            client_outStream=tempclient_outStream;
            client_inStream=tempclient_inStream;
            client.setSoTimeout(10);

            byMng=new ByteManager(flgCarr);
            ipAddressOfPC=ipAddress;

            UIhnd.startApp(dialogConn);
        }
        catch (IOException e)
        {

            while(Locker.isLocked()){}

            if (!flgCarr.S_ifConnected())
            {
                errMng.error_connectingPC(e.getMessage(),UIhnd);
                flgCarr.T_checkAddr_Available();
            }
        }
    }

    private void checkResponse_init(BufferedInputStream tempCl_inStream,BufferedOutputStream tempCl_outStream,String ipAddress)throws IOException{


        if (dataAvailable(tempCl_inStream))
        {
            byte[] bytes = new byte[tempCl_inStream.available()];
            tempCl_inStream.read(bytes);

            String temp_nameOfPC = new String(bytes);

            if (temp_nameOfPC.contains("6392")) {

                temp_nameOfPC = (temp_nameOfPC.length() > 4) ? temp_nameOfPC.substring(4) : ipAddress;

                checkResponse(tempCl_outStream,tempCl_inStream,temp_nameOfPC);
            }
            else throw new IOException("Error1: Wrong respond from server");
        }
    }


    private void checkResponse(BufferedOutputStream tempCl_outStream,BufferedInputStream tempCL_inStream,String temp_nameOfPC)throws IOException{

        while(Locker.isLocked()) {}

        if (flgCarr.S_ifConnected())
        {
            tempCl_outStream.close();
            throw new IOException("Error2:Already connected to PC");
        }

        try
        {
            Locker.lock();

            dtHld.sendRequest(6392,temp_nameOfPC);
            String response;

            while (true) {

                response = (String) dtHld.waitResponse(6391);

                if (response != null && response.contains("next"))
                {
                    tempCl_outStream.close();
                    throw new IOException("Info: Searching another...");

                }
                else if (response != null && response.contains("connect :"))
                {
                    passwordCheck("Password:".concat(response.substring(response.indexOf(':')+1)),tempCl_outStream,tempCL_inStream);
                    break;
                }
            }
        }
        finally
        {
            Locker.unlock();
        }
    }

    private void passwordCheck(String password,BufferedOutputStream tempCl_outStream,BufferedInputStream tempCl_inStream)throws IOException{

        tempCl_outStream.write(password.concat("*").getBytes());
        tempCl_outStream.flush();

        if (dataAvailable(tempCl_inStream))
        {
            if (tempCl_inStream.read()==(byte)1)
            {
                passwordOfPC=password;
                flgCarr.S_Connect();
            }
            else
            {
                tempCl_outStream.close();
                throw new IOException("Info:Wrong password");
            }
        }
    }


    /**
     *
     *
     * FIRST SECTION PART TWO------------------------------CONNECT PC---------------------------------------------------------------
     *
     *
     * */


    public void connectServer_init(Flag_Carrier flgCr,
                                   ThreadManager_dataHolder dataHld,
                                   UI_main UI_h,
                                   ErrorManager_generic err,
                                   Dialog_connection dialogConn,
                                   String ipAddress,
                                   String password)
    {
        errMng=err;
        flgCarr=flgCr;
        UIhnd =UI_h;
        dtHld=dataHld;

        connect(ipAddress,password,dialogConn);

    }

    private void connect(String ipAddress,String password,Dialog_connection dialogConn)
    {
        try
        {
            client = new Socket(ipAddress, 3000);
            client_inStream=new BufferedInputStream(client.getInputStream());
            client_outStream=new BufferedOutputStream(client.getOutputStream());

            client_outStream.write("Password:".concat(password.concat("*")).getBytes());
            client_outStream.flush();

            checkResponse_init(dialogConn,password,ipAddress);
            UIhnd.Loading_dismiss();
        }
        catch(IOException e)
        {
            UIhnd.Loading_dismiss();
            errMng.error_connectingPC(e.getMessage(),UIhnd);
            flgCarr.T_connectPC_Deactivate();
        }
    }


    private void checkResponse_init(Dialog_connection dialogConn,String password,String ipAddress)throws IOException
    {
        if (dataAvailable())
        {
            if (client_inStream.read()==(byte)1)
            {
                passwordOfPC=password;
                ipAddressOfPC=ipAddress;
                client.setSoTimeout(10);

                byMng=new ByteManager(flgCarr);


                flgCarr.T_connectPC_Deactivate();
                UIhnd.startApp(dialogConn);
            }
            else
            {
                client.close();
                flgCarr.T_connectPC_Deactivate();
                UIhnd.Toast(R.string.Toast_Wrong_credentials, Toast.LENGTH_SHORT);
            }
        }

    }


    private boolean dataAvailable(BufferedInputStream tempCl_inStream)throws IOException{

        for (int i=0; i<250;i++)
        {
            if (tempCl_inStream.available()>0)return true;

            UIhnd.sleep(50);
        }
        tempCl_inStream.close();
        throw new IOException("Error0: No response from server");
    }


    private boolean dataAvailable()throws IOException{

        for (int i=0; i<250;i++)
        {
            if (client_inStream.available()>0)return true;

            UIhnd.sleep(50);
        }
        client.close();
        throw new IOException("Info: No response from server");
    }


/***-------------------------------------------------RECONNECT-------------------------------------------------------------------*/

    public boolean reconnect()
    {
        if (client==null) return false;

        try
        {
            client=new Socket();
            client.connect(new InetSocketAddress(ipAddressOfPC,3000),10000);
            client.setSoTimeout(10);

            client_outStream=new BufferedOutputStream(client.getOutputStream());
            client_inStream=new BufferedInputStream(client.getInputStream());

            return true;
        }
        catch (IOException e)
        {
            return false;
        }
    }

/**---SECOND SECTION--------------------------------COMMUNICATION---------------------------------------------------------------*/


    public void sendRequest_init(FileManager flMng)
    {

        try
        {
            byMng.cleanPipe(client_inStream);

            switch (flgCarr.S_communicationRequest_getRequest())
            {

                case S_COMMUNICATION_LOADROOT:loadRoot((FileManager_pc) flMng);break;

                case S_COMMUNICATION_OPEN:open((FileManager_pc) flMng);break;

                case S_COMMUNICATION_CLOSE:close((FileManager_pc) flMng);break;

                case S_COMMUNICATION_CLICKFILE:clickedFile((FileManager_pc) flMng);break;

                case S_COMMUNICATION_CLICKALL:clicked_selectAll((FileManager_pc) flMng);break;

                case S_COMMUNICATION_CHOOSEFOLDER:chooseDwnFolder();break;

                case S_COMMUNICATION_CREATEFOLDER:createDwnFolder((FileManager_pc) flMng);break;

                case S_COMMUNICATION_DOWNLOAD:download_init((FileManager_pc) flMng);break;

                case S_COMMUNICATION_UPLOAD:upload_init((FileManager_android)flMng);break;

                case S_COMMUNICATION_STATUS_NSUCCESS:status_nSuccess((FileManager_pc) flMng);break;

                case S_COMMUNICATION_START:start();break;

                case S_COMMUNICATION_STOP:stop();break;

                case S_COMMUNICATION_ERROR_OVERFLOW:errorOverflow_CommunicationPC((FileManager_pc) flMng);break;

                default:
                    closingCommunication_complete();break;

            }
        }
        catch(IOException e)
        {
            errMng.error_communication(e.getMessage(),this ,UIhnd,flgCarr,client_outStream,flMng,dtHld);
        }
    }


    private void loadRoot(FileManager_pc flMng) throws IOException
    {

        send("ROOT*".getBytes());

        if (byMng.recvResp(client_inStream,"0","-1",10000))
        {
            flMng.clear();
            flMng.organizeRecvData(byMng.getResponse());
            byMng.cleanBuilder();

            UIhnd.closeDrawer_left();
            UIhnd.notifyChange();

            errMng.errCnt.success(1);
            closingCommunication_complete();
        }
    }


    private void open(FileManager_pc flMng)throws IOException
    {

        if (dtHld.waitRequest(6400))
        {
            Integer position=(Integer)dtHld.getSharedData();

            String name=flMng.getName(position,0);

            send("OPEN".concat(position.toString()).concat("*").getBytes());

            if (byMng.recvResp(client_inStream,"1",position.toString(),5000))
            {
                if (byMng.getResponse().contains("<DOWNLOAD FOLDER>"))name=name.concat(" (Download folder)");

                flMng.clear();
                flMng.organizeRecvData(byMng.getResponse());
                byMng.cleanBuilder();

                UIhnd.changeURL(flMng,name,R.id.text_url_fragmentPC);
                UIhnd.notifyChange();

                errMng.errCnt.success(1);
                closingCommunication_complete();
            }
        }
    }


    private void close (FileManager_pc flMng) throws IOException
    {
        send("CLOSE*".getBytes());

        if (byMng.recvResp(client_inStream,"2","-1",5000))
        {

            flMng.clear();
            flMng.organizeRecvData(byMng.getResponse());
            byMng.cleanBuilder();

            UIhnd.changeURL(flMng,R.id.text_url_fragmentPC);
            UIhnd.notifyChange();

            errMng.errCnt.success(1);
            closingCommunication_complete();
        }
    }

    private void clickedFile(FileManager_pc flMng) throws IOException
    {
        if (dtHld.waitRequest(6401))
        {
            Integer position=(Integer)dtHld.getSharedData();

            send("CLICKED".concat(position.toString().concat("*")).getBytes());

            if (byMng.recvResp(client_inStream,"3",position.toString(),5000))
            {
                if (byMng.getResponse().equals("ADDED*")) flMng.selectOne(position);

                else if (byMng.getResponse().equals("REMOVED*")) flMng.removeOne(position);

                byMng.cleanBuilder();
                UIhnd.notifyChange();

                errMng.errCnt.success(1);
                closingCommunication_complete();
            }
        }
    }

    private void clicked_selectAll(FileManager_pc flMng) throws IOException
    {
        send("SELECTALL*".getBytes());

        if (byMng.recvResp(client_inStream,"4","-1",10000))
        {
            if (byMng.getResponse().equals("ADDED*")) flMng.selectAll();

            else if (byMng.getResponse().equals("REMOVED*")) flMng.removeAll();

            byMng.cleanBuilder();
            UIhnd.notifyChange();

            errMng.errCnt.success(1);
            closingCommunication_complete();
        }
    }

    private void chooseDwnFolder()throws IOException
    {
        send("CHOOSEFOLDER*".getBytes());

        if (byMng.recvResp(client_inStream,"5","-1",5000))
        {
            if (byMng.getResponse().equals("CHANGED*"))UIhnd.Toast(R.string.Toast_DwnFolder_changedNoName,Toast.LENGTH_SHORT);

            else UIhnd.Toast(R.string.Error_changeDwnFolder,Toast.LENGTH_SHORT);

            byMng.cleanBuilder();

            errMng.errCnt.success(1);
            closingCommunication_complete();

        }
    }

    private void createDwnFolder(FileManager_pc flMng)throws IOException
    {
        send("CREATEFOLDER*".getBytes());

        if (byMng.recvResp(client_inStream,"6","-1",5000))
        {
            if (byMng.getResponse().equals("NCREATED*"))UIhnd.Toast(R.string.Error_createDwnFolder,Toast.LENGTH_SHORT);

            else
            {
                flMng.clear();
                flMng.organizeRecvData(byMng.getResponse());
                UIhnd.notifyChange();
            }
            byMng.cleanBuilder();
            errMng.errCnt.success(1);
            closingCommunication_complete();
        }
    }



    private void download_init(FileManager_pc flMng) throws  IOException
    {
        send("DOWNLOAD*".getBytes());

        byMng.recvFiles(client_inStream,client_outStream,flMng.getDwnlFolder(),UIhnd,flMng.getSlcItemCount());

        flMng.reset();
        UIhnd.notifyChange();
        closingCommunication_complete();
    }

    private void upload_init(FileManager_android flMng) throws IOException
    {
        send("UPLOAD*".getBytes());

        byMng.sendFiles(client_inStream,client_outStream,flMng,UIhnd);

        flMng.getCart().clear();
        UIhnd.notifyChange();
        closingCommunication_complete();

    }




    private void send(byte[] bytes) throws IOException
    {
        try
        {
            client_outStream.write(bytes);
            client_outStream.flush();
        }
        catch(IOException e)
        {
            throw new IOException("Error:PIPE broken:"+new String(bytes));
        }
    }




    /**
     * Connection controlling functions
     *
     * */

    private void status_nSuccess(FileManager_pc flMng)
    {
        try
        {
            if(dtHld.waitRequest(5999))
            {
                String status=(String)dtHld.getSharedData();

                if (!status.equals("NSEND")) send(status.getBytes());

                if (byMng.recvResp(client_inStream, "-1", "-1",5000))
                {
                    byMng.cleanBytes();
                    byMng.cleanBuilder();

                    errMng.errCnt.success(1);
                    closingCommunication_complete();
                }
            }
        }
        catch (IOException e)
        {
            errMng.error_communication("Error:nStatus not recivied",this ,UIhnd,flgCarr,client_outStream,flMng,dtHld);
        }
    }

    private void errorOverflow_CommunicationPC(FileManager_pc flMng)
    {
        try
        {
            byMng.cleanBytes();
            byMng.cleanBuilder();

            send("MEMORY LOST*ROOT*".getBytes());

            if (byMng.recvResp(client_inStream, "0", "-1",10000))
            {
                flMng.clear();
                flMng.resetPath();
                flMng.organizeRecvData(byMng.getResponse());
                byMng.cleanBuilder();

                UIhnd.notifyChange();
                UIhnd.Toast(R.string.Toast_MemoryLost_reset,Toast.LENGTH_SHORT);

                errMng.errCnt.success(1);
                closingCommunication_complete();
            }
        }
        catch(IOException e)
        {
            errMng.errCnt.success(1);
            errMng.error_communication("Error:Complete restart",this ,UIhnd,flgCarr,client_outStream,flMng,dtHld);
        }
    }


    private void stop()throws IOException
    {
        try
        {
            send("STOP*".getBytes());
            closingCommunication_complete();
        }
        catch(IOException e)
        {
            throw new IOException("Error:stop request was not sent");
        }
    }

    private void start() throws IOException
    {
        try
        {
            send("START*".getBytes());
            closingCommunication_complete();
        }
        catch(IOException e)
        {
            throw new IOException("Error: start request was not sent");
        }
    }

    public void killSocket()
    {
        try
        {
            client.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }



    public void closingCommunication_complete()
    {
        flgCarr.T_Communication_nActive();
        flgCarr.S_communicationRequest_nRequest();
        UIhnd.Loading_dismiss();
    }

    public void closingCommunication_thread()
    {
        flgCarr.T_Communication_nActive();
    }

    public void closingCommunication_request()
    {
        flgCarr.S_communicationRequest_nRequest();
    }

    public void closingCommunication_dialog()
    {
        UIhnd.Loading_dismiss();
    }


    public void pipeStuck()
    {
        byMng.pipeStuck();
    }






}
