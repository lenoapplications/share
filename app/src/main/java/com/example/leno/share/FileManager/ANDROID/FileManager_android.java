package com.example.leno.share.FileManager.ANDROID;

/**
 * Internal imports
 * */
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.util.Log;

import com.example.leno.share.Enums.FileType;
import com.example.leno.share.FileManager.Data;
import com.example.leno.share.FileManager.FileCart;
import com.example.leno.share.FileManager.FileManager;
import com.example.leno.share.R;
import com.example.leno.share.UI_Manager.UI_handler.UI_main;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Android imports
 *
 * */

/**
 * Created by LeNo on 25.9.2017..
 */

public class FileManager_android extends FileManager {


    private FileCart selcItm;
    private Data_android root;
    private UI_main UIhnd;

    private File downloadFolder;

    public FileManager_android(UI_main UIh,Activity act)
    {
        files=new ArrayList<>();
        UIhnd=UIh;
        selcItm=new FileCart();
        checkDwnFolder(act);
    }

    private void checkDwnFolder(Activity activity)
    {
        SharedPreferences prefs=activity.getSharedPreferences("tempData", Context.MODE_PRIVATE);

        String absPath;

        if ((absPath=prefs.getString("tempDwnFolderPath",null))!=null)downloadFolder=checkDwnFolderExist(new File(absPath));

    }

    private File checkDwnFolderExist(File dwnFolder)
    {
        return (dwnFolder.exists())? dwnFolder:null;
    }


    public void roots_init()
    {
        File[] roots=Environment.getExternalStorageDirectory().listFiles();

        root=null;

        for (File file:roots) addFile(file,null);

        UIhnd.notifyChange();
    }


    public void openFolder(Data_android fldr)
    {
        root=fldr;

        if (downloadFolder!=null && root.getAbsPath().equals(downloadFolder.getAbsolutePath()))
            UIhnd.changeURL(this,fldr.getName(0).concat(" (Download Folder)"),R.id.text_url_fragmentANDROID);


        else UIhnd.changeURL(this,fldr.getName(0),R.id.text_url_fragmentANDROID);

        files.clear();

        for (String absPath:fldr.listFiles()) addFile(absPath,root);

        UIhnd.notifyChange();
        UIhnd.Loading_dismiss();
    }


    public void closeFolder()
    {
        try
        {
            files.clear();

            UIhnd.changeURL(this,R.id.text_url_fragmentANDROID);

            root = root.getRoot();

            for (String absPath : root.listFiles()) addFile(absPath, root);

        }
        catch (NullPointerException e)
        {
            roots_init();
        }
        finally
        {
            UIhnd.notifyChange();
            UIhnd.Loading_dismiss();
        }
    }

    public void reloadeRoot()
    {
        try
        {
            files.clear();

            for (String absPath : root.listFiles()) addFile(absPath, root);
        }
        catch (NullPointerException e)
        {
            roots_init();
        }
        finally
        {
            UIhnd.notifyChange();
        }
    }


    private void addFile(File fl,Data_android rt)
    {
        Data_android newData=new Data_android(this,fl.getAbsolutePath(),rt);

        files.add(newData);
    }

    private void addFile(String absPath,Data_android rt)
    {
        Data_android newData=new Data_android(this,absPath,rt);

        files.add(newData);
    }




    public Data_android getRoot()
    {
        return root;
    }

    public File getDwnFolder()
    {
        return downloadFolder;
    }

    public FileCart getCart()
    {
        return selcItm;
    }

    public void fileSelected(Data_android data)
    {
        selcItm.onClickFile(data);
    }

    public boolean isFileSelected(Data_android data)
    {
        return selcItm.contains(data);
    }

    public void selectAll()
    {
       for (Data data:files)
       {
           if (((Data_android)data).type==FileType.DIRECTORY)continue;

           if (!selcItm.onClickFile((Data_android)data))continue;

           else
           {
               selcItm.selectAll(files);
               break;
           }
       }
    }

    public String changeDwnFolder()
    {
        if (root!=null)
        {
            downloadFolder = new File(root.getAbsPath());
            return root.getAbsPath();
        }
        else
        {
            downloadFolder=createDefaultDwnlFolder(Environment.getExternalStorageDirectory());
            return downloadFolder.getAbsolutePath();
        }
    }

    public File createDefaultDwnlFolder(File dir)
    {
        File dirFile=new File(dir,"PC downloads ");

        if (!dirFile.exists())dirFile.mkdir();

        return dirFile;
    }



}
