package com.example.leno.share.ErrorManager;


/**
 *
 * Internal imports
 *
 * */

import com.example.leno.share.Communication.Client;
import com.example.leno.share.Enums.ServerStatus;
import com.example.leno.share.FileManager.FileManager;
import com.example.leno.share.FileManager.PC.FileManager_pc;
import com.example.leno.share.R;
import com.example.leno.share.ThreadManager.Flag_Carrier;
import com.example.leno.share.ThreadManager.ThreadManager_dataHolder;
import com.example.leno.share.ThreadManager.ThreadManager_generic;
import com.example.leno.share.UI_Manager.UI_handler.UI_main;
import com.example.leno.share.app;

/**
 *
 * Android imports
 * */
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;


/**
 * Java imports
 *
 * */
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.util.Collections;

/**
 * Created by LeNo on 5.9.2017..
 *
 * purpose of class:
 *          - catch all errors in one place,so it can be easier to handle
 *
 *
 *          Description->
 *                  -> first section
 *                      -contains functions for handling errors that appear in "searchPC" address section in "Client" java class.
 *                  -> second section
 *                      -contains functions for handling errors that appear in "communication" (sending requests,recvResp.response)
 *
 *        REQUESTS:
 *              0-> load roots request
 *              1 ->open folder/root request
 *
 */

public class ErrorManager_generic {

    public final ErrorManager_counter errCnt;
    private final ThreadManager_generic thMng;
    private final app app;

    public ErrorManager_generic(ThreadManager_generic tM, app a)
    {
        errCnt=new ErrorManager_counter();
        thMng = tM;
        app = a;
    }



    public void error_connectingPC(String errMsg, Object... args)
    {
        if (errMsg.contains("Info:"))
        {
            ((UI_main) args[0]).Toast(R.string.Error_Empty, Toast.LENGTH_SHORT, errMsg.substring(errMsg.indexOf(":") + 1));
        }

        else
        {
            Log.d("Error",errMsg);
        }
    }



    public void error_communication(String msg, Object... args)
    {
        try {
            Log.d("Error",msg);
            errCnt.nsuccess(1);

            if (errCnt.errorOverflow_communicationPC())
            {
                error_communicationOverflow_CommunicationPC((Client)args[0],(Flag_Carrier)args[2],(FileManager_pc)args[4]);
            }

            else if (msg.matches("\\bError\\d+\\b:timeout"))
            {
                error_communication_responseTimeout(    (Client) args[0],
                                                        (FileManager_pc)args[4],
                                                        (UI_main) args[1],
                                                        (ThreadManager_dataHolder) args[5],
                                                        (Flag_Carrier)args[2],
                                                         Integer.parseInt(msg.substring(5, 6)));
            }

            else if (msg.matches("\\bError\\d+\\b:interrupted"))
            {
                error_communication_interrupted( (Client) args[0],
                                                 (UI_main) args[1],
                                                 (BufferedOutputStream) args[3],
                                                 Integer.parseInt(msg.substring(5, 6)));
            }

            else if (msg.matches("\\bError\\d+\\b:server closed:(-1|\\d+)"))
            {
                error_communication_serverClosed(   (Client) args[0],
                                                    (FileManager_pc) args[4],
                                                    (ThreadManager_dataHolder) args[5],
                                                    Integer.parseInt(msg.substring(5, 6)),
                                                    Integer.parseInt(msg.substring(msg.lastIndexOf(':') + 1)));
            }

            else if (msg.matches("Error:nStatus not recivied"))
            {
                error_communication_nSuccessTimeout((Client)args[0],
                                                    (FileManager_pc)args[4],
                                                    (ThreadManager_dataHolder)args[5]);
            }


            else if (msg.matches("Error:stop request was not sent|Error:start request was not sent"))
            {
                error_communication_startStop((Client) args[0], (FileManager_pc) args[4]);
            }


            else if (msg.contains("Error:PIPE broken"))
            {
                error_brokenPipe((Client)args[0],
                                (ThreadManager_dataHolder)args[5],
                                (FileManager_pc)args[4],
                                 msg.substring(msg.lastIndexOf(":")+1));
            }

            else if (msg.contains("Error:download operation brokedown"))
            {
                error_communication_download((Client)args[0],(FileManager_pc)args[4],(UI_main)args[1]);
            }

            else throw new IOException("Error:Complete restart");
        }

        catch (IOException e)
        {
            ((UI_main) args[1]).Toast(R.string.Toast_Connection_crashed, Toast.LENGTH_SHORT);

            app.finish();

            Intent i = app.getBaseContext().getPackageManager().getLaunchIntentForPackage(app.getBaseContext().getPackageName());
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            app.startActivity(i);
        }
    }


    private void error_communication_responseTimeout(Client client,
                                                     FileManager_pc flMngPc,
                                                     UI_main UIhnd,
                                                     ThreadManager_dataHolder dtHld,
                                                     Flag_Carrier flgCarr,
                                                     int request) throws IOException
    {
        switch (request)
        {
            case 0:
                {
                    client.pipeStuck();
                    UIhnd.destroyFragment(R.string.Error_Timeout);
                    client.closingCommunication_complete();
                    break;
                }

            case 1:
                {
                    dtHld.sendRequest(5999,"OPENSTATUS_NSUCCESS*");
                    client.closingCommunication_thread();
                    flgCarr.S_communicationRequest_addRequest(ServerStatus.S_COMMUNICATION_STATUS_NSUCCESS);
                    thMng.thread_Init(client, "sendRequest_init", flMngPc, FileManager.class, 2);
                    break;
                }

            case 2:
                {
                    dtHld.sendRequest(5999,"CLOSESTATUS_NSUCCESS*");
                    client.closingCommunication_thread();
                    flgCarr.S_communicationRequest_addRequest(ServerStatus.S_COMMUNICATION_STATUS_NSUCCESS);
                    thMng.thread_Init(client, "sendRequest_init", flMngPc, FileManager.class, 2);
                    break;
                }

            case 3:
                {
                    dtHld.sendRequest(5999,"CLICKEDSTATUS_NSUCCESS*");
                    client.closingCommunication_thread();
                    flgCarr.S_communicationRequest_addRequest(ServerStatus.S_COMMUNICATION_STATUS_NSUCCESS);
                    thMng.thread_Init(client,"sendRequest_init",flMngPc,FileManager.class,2);
                }
            case 4:
                {
                    dtHld.sendRequest(5999,"CLICKEDALLSTATUS_NSUCCESS*");
                    client.closingCommunication_thread();
                    flgCarr.S_communicationRequest_addRequest(ServerStatus.S_COMMUNICATION_STATUS_NSUCCESS);
                    thMng.thread_Init(client,"sendRequest_init",flMngPc,FileManager.class,2);
                }
            case 5:
                {
                    dtHld.sendRequest(5999,"CHOOSEFOLDER_NSUCCESS*");
                    client.closingCommunication_thread();
                    flgCarr.S_communicationRequest_addRequest(ServerStatus.S_COMMUNICATION_STATUS_NSUCCESS);
                    thMng.thread_Init(client,"sendRequest_init",flMngPc,FileManager.class,2);
                }
            case 6:
                {
                    dtHld.sendRequest(5999,"CREATEFOLDER_NSUCCESS*");
                    client.closingCommunication_thread();
                    flgCarr.S_communicationRequest_addRequest(ServerStatus.S_COMMUNICATION_STATUS_NSUCCESS);
                    thMng.thread_Init(client,"sendRequest_init",flMngPc,FileManager.class,2);
                }
        }
    }

    private void error_communication_nSuccessTimeout(Client client,
                                                     FileManager_pc flMngPc,
                                                     ThreadManager_dataHolder dtHld)
    {

        client.pipeStuck();
        client.closingCommunication_thread();

        dtHld.sendRequest(5999,"NSEND");
        thMng.thread_Init(client, "sendRequest_init", flMngPc, FileManager.class, 2);

    }


    private void error_communication_interrupted(Client client, UI_main UIhnd, BufferedOutputStream outStr, int request) throws IOException
    {
        client.pipeStuck();
        switch (request)
        {
            case 0:
                {
                    UIhnd.destroyFragment();
                    client.closingCommunication_complete();
                    break;
                }

            case 1:
                {
                    outStr.write("OPENSTATUS_NSUCCESS*".getBytes());
                    outStr.flush();
                    client.closingCommunication_complete();
                    break;
                }

            case 2:
                {
                    outStr.write("CLOSESTATUS_NSUCCESS*".getBytes());
                    outStr.flush();
                    client.closingCommunication_complete();
                    break;
                }
        }
    }


    private void error_communication_serverClosed(Client client,
                                                  FileManager_pc flMngPc,
                                                  ThreadManager_dataHolder dtHld,
                                                  int request,
                                                  int position)
    {
        if (client.reconnect())
        {
            client.closingCommunication_thread();

            if (request==1)dtHld.sendRequest(6400,position);

            thMng.thread_Init(client, "sendRequest_init", flMngPc, FileManager.class, 2);

        }
        else
        {
            client.killSocket();
            client.closingCommunication_complete();
        }
    }



    private void error_brokenPipe(Client client,ThreadManager_dataHolder dtHld,FileManager_pc flMngPc,String request)throws IOException
    {
        client.killSocket();

        if (client.reconnect())
        {
            client.closingCommunication_thread();

            if(request.contains("OPEN")) dtHld.sendRequest(6400,Integer.parseInt(request.substring(4,request.indexOf('*'))));

            thMng.thread_Init(client, "sendRequest_init", flMngPc, FileManager.class, 2);
        }
        else
        {
            throw new IOException("Complete restart app");
        }
    }

    private void error_communication_startStop(Client client,FileManager_pc flMngPc)throws IOException
    {
        if (client.reconnect())
        {
            client.closingCommunication_thread();

            thMng.thread_Init(client, "sendRequest_init", flMngPc, FileManager.class, 2);
        }

        else throw new IOException("Complete restart app");
    }


    private void error_communication_download(Client client,FileManager_pc flMngPc,UI_main UIhnd)throws IOException
    {
        if(client.reconnect())
        {
            flMngPc.reset();
            UIhnd.notifyChange();
            client.pipeStuck();
            client.closingCommunication_complete();
        }
        else throw new IOException("Complete restart app");

    }


    private void error_communicationOverflow_CommunicationPC(Client client,Flag_Carrier flgCarr,FileManager_pc flMngPc)
    {
        client.closingCommunication_thread();
        client.pipeStuck();

        flgCarr.S_communicationRequest_addRequest(ServerStatus.S_COMMUNICATION_ERROR_OVERFLOW);
        thMng.thread_Init(client, "sendRequest_init", flMngPc, FileManager.class, 2);
    }
}
