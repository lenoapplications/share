package com.example.leno.share.UI_Manager.Fragments;



/**
 * Internal imports
 * */
import com.example.leno.share.Communication.Client;
import com.example.leno.share.Enums.ServerStatus;
import com.example.leno.share.ErrorManager.ErrorManager_generic;
import com.example.leno.share.FileManager.FileManager;
import com.example.leno.share.FileManager.PC.Data_pc;
import com.example.leno.share.FileManager.PC.FileManager_pc;
import com.example.leno.share.R;
import com.example.leno.share.ThreadManager.Flag_Carrier;
import com.example.leno.share.ThreadManager.ThreadManager_dataHolder;
import com.example.leno.share.ThreadManager.ThreadManager_generic;
import com.example.leno.share.UI_Manager.UI_handler.UI_main;
/**
 *
 * Android imports
 *
 * */
import android.content.Context;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;


/**
 * Created by LeNo on 16.9.2017..
 *
 *
 * Purpose of class :
 *          displaying files and folders.
 *
 */

public class PC_fragment extends Fragment {

    private boolean CREATED=false;

    private ThreadManager_dataHolder dtHld;
    private ThreadManager_generic thdMng;

    private Flag_Carrier flgCarr;
    private UI_main UIhnd;
    private Client client;
    private ErrorManager_generic errMng;
    private FileManager_pc flMngPc;


    private GridAdapter_pc adpt;

    public void objects_init(ThreadManager_dataHolder thD,
                       ThreadManager_generic thM,
                       Flag_Carrier flgC,
                       UI_main UIh,
                       Client cl,
                       ErrorManager_generic errM,
                       FileManager_pc flMpc)
    {
        dtHld=thD;
        thdMng=thM;
        flgCarr=flgC;
        UIhnd=UIh;
        client=cl;
        errMng=errM;
        flMngPc=flMpc;

    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view=inflater.inflate(R.layout.fragment_pc,container,false);

        gridView_init(view);

        buttons_init(view);

        if (!CREATED) root_init();

        else UIhnd.closeDrawer_left();

        return view;
    }


    private void buttons_init(View view)
    {
        ImageButton selectAll=(ImageButton)view.findViewById(R.id.buttonImage_selectAll_pc);
        ImageButton chooseDwnLc=(ImageButton) view.findViewById(R.id.buttonImage_chooseDwnFolder_pc);
        ImageButton createFolder=(ImageButton) view.findViewById(R.id.buttonImage_createFolder_pc);

        Button send=(Button) view.findViewById(R.id.button_download_pc);

        selectAll.setOnClickListener(selectAll_onClickListener());
        chooseDwnLc.setOnClickListener(chooseDwnFolder_onClickListener());
        createFolder.setOnClickListener(createDwnFolder_onClickListener());

        send.setOnClickListener(download_onClickListener());
    }

    private View.OnClickListener selectAll_onClickListener()
    {
        return new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                UIhnd.Loading_show(R.string.loading,UIhnd,flgCarr);
                flgCarr.S_communicationRequest_addRequest(ServerStatus.S_COMMUNICATION_CLICKALL);
                thdMng.thread_Init(client,"sendRequest_init",flMngPc,FileManager.class,2);
            }
        };
    }


    private View.OnClickListener chooseDwnFolder_onClickListener()
    {
        return new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                UIhnd.Loading_show(R.string.loading,UIhnd,flgCarr);
                flgCarr.S_communicationRequest_addRequest(ServerStatus.S_COMMUNICATION_CHOOSEFOLDER);
                thdMng.thread_Init(client, "sendRequest_init", flMngPc, FileManager.class, 2);
            }
        };
    }

    private View.OnClickListener createDwnFolder_onClickListener()
    {
        return new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                UIhnd.Loading_show(R.string.loading,UIhnd,flgCarr);
                flgCarr.S_communicationRequest_addRequest(ServerStatus.S_COMMUNICATION_CREATEFOLDER);
                thdMng.thread_Init(client, "sendRequest_init", flMngPc, FileManager.class, 2);
            }
        };
    }

    private View.OnClickListener download_onClickListener()
    {
        return new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                if (flMngPc.getDwnlFolder()!=null)
                {
                    UIhnd.Loading_show(R.string.downloading, UIhnd, flgCarr, "0", "0");
                    flgCarr.S_communicationRequest_addRequest(ServerStatus.S_COMMUNICATION_DOWNLOAD);
                    thdMng.thread_Init(client, "sendRequest_init", flMngPc, FileManager.class, 2);
                }
                else UIhnd.Toast(R.string.Toast_DwnFolder_isNull, Toast.LENGTH_SHORT);
            }
        };
    }

    private void root_init()
    {
        UIhnd.Loading_show(R.string.loading,UIhnd,flgCarr);
        flgCarr.S_communicationRequest_addRequest(ServerStatus.S_COMMUNICATION_LOADROOT);
        thdMng.thread_Init(client,"sendRequest_init",flMngPc,FileManager.class,2);

        CREATED=true;
    }


    class GridAdapter_pc extends BaseAdapter {

        class DataHolder {
            TextView text;
            ImageView imageFileType;
            ImageView imageCheck;
        }

        private GridAdapter_pc.DataHolder holder;
        private GridView gView;

        public GridAdapter_pc(GridView gV)
        {
            gView=gV;
        }


        @Override
        public int getCount()
        {
            return flMngPc.getSize();
        }

        @Override
        public Object getItem(int position)
        {
            return flMngPc.files.get(position);
        }

        @Override
        public long getItemId(int position)
        {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            View row = convertView;

            if (row == null) row = row_init(parent);

            else holder = (GridAdapter_pc.DataHolder) row.getTag();

            imageLoad(holder.imageFileType,position);

            if (flMngPc.isSelected(position))
            {
                holder.imageCheck.setImageResource(R.drawable.pngimage_checkmark);
            }
            else holder.imageCheck.setImageDrawable(null);

            return row;
        }


        private View row_init(ViewGroup parent)
        {
            View row =((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.fragment_row,parent,false);
            holder = new GridAdapter_pc.DataHolder();
            holder.text = (TextView) row.findViewById(R.id.text_fragmentRow);
            holder.imageFileType =(ImageView) row.findViewById(R.id.image_fragmentRow);
            holder.imageCheck=(ImageView) row.findViewById(R.id.image_fragmentRow_fileChoosed);

            row.setTag(holder);
            return row;
        }

        private void imageLoad(ImageView image,int position)
        {
            if (flMngPc.files.size()<1)return;

            Data_pc file=(Data_pc)getItem(position);

            scalingSize(holder);

            holder.text.setText(file.getName(1));

            switch(file.type)
            {
                case ROOT:image.setImageResource(R.drawable.pngimage_hard_disk);break;
                case DIRECTORY:image.setImageResource(R.drawable.pngimage_folder);break;
                case TXT:image.setImageResource(R.drawable.pngimage_txt);break;
                case MP3:image.setImageResource(R.drawable.pngimage_music);break;
                case MP4:image.setImageResource(R.drawable.pngimage_music);break;
                case JPEG:image.setImageResource(R.drawable.pngimage_pic);break;
                case JPG:image.setImageResource(R.drawable.pngimage_pic);break;
                case PNG:image.setImageResource(R.drawable.pngimage_pic);break;
                case PDF:image.setImageResource(R.drawable.pngimage_pdf);break;
            }
        }
        private void scalingSize(DataHolder holder)
        {
            UIhnd.sizeMng.setViewSize
                    (holder.imageFileType,(LinearLayout.LayoutParams)holder.imageFileType.getLayoutParams(),0.066f,0.066f,gView.getWidth(),gView.getHeight());

            UIhnd.sizeMng.setViewSize
                    (holder.imageCheck,(LinearLayout.LayoutParams)holder.imageCheck.getLayoutParams(),0.026f,0.026f,gView.getWidth(),gView.getHeight());

            UIhnd.sizeMng.setFontSize_text(0.056f,0.056f,gView.getWidth(),gView.getHeight(),holder.text);


        }
    }


    /**
     *
     * GridView functions
     *
     * */


    private GridView gridView_init(View view)
    {
        GridView gView=(GridView) view.findViewById(R.id.grid_fragment_pc);
        adpt=new GridAdapter_pc(gView);

        gView.setAdapter(adpt);
        gView.setHorizontalScrollBarEnabled(true);
        gView.setOnItemClickListener(onItemClickListener());
        gView.setOnItemLongClickListener(onItemLongClickListener());

        UIhnd.changeAdapter(adpt);
        UIhnd.changeFragment(this);

        return gView;
    }



    private AdapterView.OnItemLongClickListener onItemLongClickListener()
    {
        return new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
            {
                Data_pc data= (Data_pc) flMngPc.files.get(position);

                thdMng.thread_Init(data,"moveIndxs",UIhnd,UI_main.class,-1);

                return true;
            }
        };
    }


    private AdapterView.OnItemClickListener onItemClickListener()
    {

        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Data_pc data= (Data_pc) flMngPc.files.get(position);

                switch(data.type)
                {
                    case DIRECTORY: onItemClick_openFolder(position); break;
                    case ROOT: onItemClick_openFolder(position); break;
                    default:onItemClick_chooseFile(position);break;

                }
            }
        };
    }


    private void onItemClick_openFolder(Integer position)
    {

        dtHld.sendRequest(6400,position);

        UIhnd.Loading_show(R.string.open_fld_rt,UIhnd,flgCarr);

        flgCarr.S_communicationRequest_addRequest(ServerStatus.S_COMMUNICATION_OPEN);

        thdMng.thread_Init(client,"sendRequest_init",flMngPc,FileManager.class,2);
    }

    private void onItemClick_chooseFile(Integer position)
    {
        dtHld.sendRequest(6401,position);

        UIhnd.Loading_show(R.string.loading,UIhnd,flgCarr);

        flgCarr.S_communicationRequest_addRequest(ServerStatus.S_COMMUNICATION_CLICKFILE);

        thdMng.thread_Init(client,"sendRequest_init",flMngPc,FileManager.class,2);
    }




}
