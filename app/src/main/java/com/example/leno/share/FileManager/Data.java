package com.example.leno.share.FileManager;

import android.util.Log;
import android.widget.BaseAdapter;

import com.example.leno.share.Enums.FileType;
import com.example.leno.share.UI_Manager.Fragments.PC_fragment;
import com.example.leno.share.UI_Manager.UI_handler.UI_main;

/**
 * Created by LeNo on 14.9.2017..
 */

public abstract class Data {

    public  FileType type;
    public  String name;


    private int firstIndx=0;
    private int secondIndx=10;

    protected abstract FileType checkType(String name,FileManager flMng);


    public String getName(int form)
    {
        switch(form)
        {
            case 1:return (name.length()<=10)? name:name.substring(firstIndx,secondIndx).concat("...");
            default:return name;
        }
    }

    public void moveIndxs(UI_main UIhnd)
    {
        if (name.length()<=10)return;

        UIhnd.sleep(200);

        while(secondIndx!=name.length())
        {
            ++firstIndx;
            ++secondIndx;
            UIhnd.notifyChange();
            UIhnd.sleep(200);
        }

        UIhnd.sleep(1000);
        firstIndx=0;
        secondIndx=10;
        UIhnd.notifyChange();
    }
}
