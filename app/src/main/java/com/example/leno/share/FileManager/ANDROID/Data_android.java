package com.example.leno.share.FileManager.ANDROID;


import android.util.Log;

import com.example.leno.share.Enums.FileType;
import com.example.leno.share.FileManager.Data;
import com.example.leno.share.FileManager.FileManager;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * Created by LeNo on 25.9.2017..
 */

public class Data_android extends Data {


    private String absPath;
    private Data_android homeFolder;

    public Data_android(FileManager_android flMngAnd,String abP, Data_android hmFld)
    {
        absPath=abP;
        name=parseName();
        homeFolder=hmFld;
        type=checkType(name,flMngAnd);
    }

    private String parseName()
    {
        return absPath.substring(absPath.lastIndexOf("/")+1);
    }


    protected FileType checkType(String name,FileManager flMng)
    {

        if (isDir()) return FileType.DIRECTORY;

        else return specificFileType(name.substring(name.lastIndexOf(".")));

    }

    private FileType specificFileType(String extension)
    {
        switch(extension)
        {
            case(".mp3"):return FileType.MP3;

            case(".mp4"):return FileType.MP4;

            case (".jpg"):return FileType.JPG;

            case (".jpeg"):return FileType.JPEG;

            case (".png"):return FileType.PNG;

            case (".txt"):return FileType.TXT;

            case (".pdf"):return FileType.PDF;

            default:return FileType.UNKNOWN;
        }
    }




    public boolean isDir()
    {
        return new File(absPath).isDirectory();
    }

    public FileType getType()
    {
        return type;
    }

    public Data_android getRoot()
    {
        return homeFolder;
    }

    public String getAbsPath()
    {
        return absPath;
    }


    public String[] listFiles()
    {
        ArrayList<String> files=new ArrayList<>();

        try
        {
            if (type==FileType.DIRECTORY)
            {
                for (File file:new File(absPath).listFiles(new Filter()))
                {
                    files.add(file.getAbsolutePath());
                }
            }
            return files.toArray(new String[files.size()]);
        }
        catch (NullPointerException e)
        {
            return files.toArray(new String[files.size()]);
        }

    }



    class Filter implements FileFilter
    {
        private final Pattern pattern= Pattern.compile(".+\\.(mp3|mp4|jpg|png|jpeg|JPEG|JPG|txt|pdf)");

        @Override
        public boolean accept(File pathname)
        {
            if (pattern.matcher(pathname.getName()).matches())return true;

            else if (pathname.isDirectory())return true;

            else return false;
        }
    }
}
