package com.example.leno.share.FileManager;

import com.example.leno.share.FileManager.PC.Data_pc;

import java.util.ArrayList;

/**
 * Created by LeNo on 25.9.2017..
 */

public class FileManager {


    public ArrayList<Data> files;

    public StringBuilder url=new StringBuilder();

    public String getName(int position,int form)
    {
        return files.get(position).getName(form);
    }


    public void addToPath(String name)
    {
        url.append("/");
        url.append(name);
    }

    public String removeFromPath()
    {
        url.setLength( (url.length()>0) ? url.lastIndexOf("/"):0 );

        return url.substring(url.lastIndexOf("/")+1);
    }

    public void resetPath()
    {
        url.setLength(0);
    }

    public int getSize()
    {
        return files.size();
    }

}
