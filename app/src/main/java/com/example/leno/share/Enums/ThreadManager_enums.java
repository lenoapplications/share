package com.example.leno.share.Enums;

/**
 * Created by LeNo on 5.9.2017..
 *
 * Purpose of enum:
 *          Simple flags that will be used by threads to know when to stop,start process or any
 *          changes that occur and in the same time affects the running thread
 */

public enum ThreadManager_enums {


    /**
    * CONNECT THREAD->Connect to PC "flags"-> flags that tells thread it can start searching for ipAddress
     *
     *          T_CONNECTPC_ACTIVE-> thread started
     *          T_CONNECTPC_NACTIVE->no active thread for searching IP address
     *          T_CONNECTPC_INTERRUPT -> stop active thread
     *
    * */
    T_CONNECTPC_ACTIVE,
    T_CONNECTPC_NACTIVE,


    /**
     *
     * COMMUNICATION THREAD
     *
     * */
    T_COMMUNICATION_ACTIVE,
    T_COMMUNICATION_NACTIVE,
    T_COMMUNICATION_INTERRUPT,
}
