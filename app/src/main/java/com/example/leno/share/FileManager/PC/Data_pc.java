package com.example.leno.share.FileManager.PC;

/***
 * Internal imports
 */

import android.util.Log;

import com.example.leno.share.Enums.FileType;
import com.example.leno.share.FileManager.Data;
import com.example.leno.share.FileManager.FileCart;
import com.example.leno.share.FileManager.FileManager;

/**
 * Created by LeNo on 14.9.2017..
 */

public class Data_pc extends Data {


    public Data_pc(String unParsedName)
    {
        type=checkType(unParsedName,null);
        name=unParsedName.substring(1);
    }


    @Override
    protected FileType checkType(String unParsedName, FileManager flMng) {

        switch(unParsedName.charAt(0))
        {
            case '|':return FileType.ROOT;
            case '<':return FileType.DIRECTORY;
            case '>':return specificFileType(unParsedName.substring(unParsedName.lastIndexOf('.')));
        }
        return null;
    }

    private FileType specificFileType(String extension)
    {
        switch(extension)
        {
            case(".mp3"):return FileType.MP3;

            case(".mp4"):return FileType.MP4;

            case (".jpg"):return FileType.JPG;

            case (".jpeg"):return FileType.JPEG;

            case (".png"):return FileType.PNG;

            case (".txt"):return FileType.TXT;

            case (".pdf"):return FileType.PDF;

            default:return FileType.UNKNOWN;
        }
    }
}
