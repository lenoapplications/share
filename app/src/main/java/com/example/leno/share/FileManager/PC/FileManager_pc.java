package com.example.leno.share.FileManager.PC;

/**
 * Internal imports
 * */
import com.example.leno.share.Enums.FileType;
import com.example.leno.share.FileManager.FileManager;
/**
 *
 * Android import
 * */
import android.util.Log;

/***
 * Java imports
 */
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by LeNo on 14.9.2017..
 *
 * Purpose of class:
 *              manages downloaded "string" that contains files and folders from PC
 *
 *
 *              Description:
 *                  organizeRecvData function->
 *                                  -it takes string argument(data)
 *                                  -finds the index of char (| or > or < or *)
 *                                  -to files is then added the "Data object"
 *                                   that takes string file(substring(0,index of char)) as argument
 *                                  Example:
 *                                         data="|C|D"
 *                                         when extracting result is -> "|C "and "|D"
 */

public class FileManager_pc extends FileManager{

    private File dwnlFolder;
    public ArrayList<Integer> selcItm;

    private int selcItmCount=0;

    public FileManager_pc()
    {
        files=new ArrayList<>();
        selcItm =new ArrayList<>();
    }



    public void organizeRecvData(String data)
    {
        if (data.contains("<DOWNLOAD FOLDER>"))data=data.substring(data.indexOf("<DOWNLOAD FOLDER>")+17);

        int last_index=findFileEnd(data.toCharArray());

        while(last_index!=0)
        {
            files.add(new Data_pc(checkFileStatus(data.substring(0,last_index))));

            data=data.substring(last_index);

            last_index=findFileEnd(data.toCharArray());
        }
    }

    private String checkFileStatus(String file)
    {
        int index;
        if ((index=file.indexOf(':'))==file.length()-1)
        {
            selcItm.add(1);
            return file.substring(0,index);
        }
        else
        {
            selcItm.add(-1);
            return file;
        }
    }

    private int findFileEnd(char[] charArray)
    {
        for (int index=1; index<charArray.length; index++)
        {
            if (charArray[index]=='|' || charArray[index]=='<'||
                charArray[index]=='>' || charArray[index]=='*') return index;
        }
        return 0;
    }


    public boolean isSelected(int position)
    {
        return selcItm.get(position)==1;
    }


    public void selectOne(int position)
    {
        ++selcItmCount;
        selcItm.set(position,1);
    }

    public void removeOne(int position)
    {
        --selcItmCount;
        selcItm.set(position,-1);
    }

    public void selectAll()
    {
        for(int i=0;i<files.size();i++)
        {
            if (files.get(i).type== FileType.DIRECTORY || files.get(i).type==FileType.ROOT)continue;

            else
            {
                if (selcItm.get(i)!=1)
                {
                    ++selcItmCount;
                    selcItm.set(i,1);
                }
            }
        }
    }

    public void removeAll()
    {
        selcItmCount-=selcItm.size();
        Collections.replaceAll(selcItm,1,-1);
    }


    public void reset()
    {
        selcItmCount=0;
        Collections.replaceAll(selcItm,1,-1);
    }

    public int getSlcItemCount()
    {
        return selcItmCount;
    }


    public void addDwnlFolder(File dwFld)
    {
        dwnlFolder=dwFld;
    }

    public File getDwnlFolder()
    {
        return dwnlFolder;
    }

    public void clear()
    {
        files.clear();
        selcItm.clear();
    }

}
