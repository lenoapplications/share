package com.example.leno.share.ThreadManager;


/**
 * Created by LeNo on 8.9.2017..
 *
 *
 * Purpose of class:
 *          class is used as a "pipeline" that shares data between threads.
 *
 *
 *
 *          Info->
 *              requestsCode:
 *                  6392 -> Used for "checkingAddress" function, password request.
 *                  6391 -> Used for "checkingAddress" function, password response.
 *                  6390 -> Used for "checkingAddress" function, end checking process.
 *
 */

public class ThreadManager_dataHolder {

    private Object sharedData;

    private int requestCode=0;

    private int responseCode=0;



    public void sendRequest(int rqCode){
        requestCode=rqCode;
    }
    public <A> void sendRequest(int rqCode,A data){

        sharedData=data;
        requestCode=rqCode;
    }

    public  boolean waitRequest(int rqCode){

        if (rqCode==requestCode)
        {
            requestCode=0;
            return true;
        }
        return false;
    }

    public <A> void sendResponse(int rsCode,A data){

        sharedData=data;
        responseCode=rsCode;
    }

    public void sendResponse(int rsCode){

        responseCode=rsCode;
    }

    public Object waitResponse(int rsCode){

        if (rsCode==responseCode)
        {
            responseCode=0;
            return getSharedData();
        }
        return null;
    }


    public <A> void addSharedData(A data){

        sharedData=data;
    }

    public Object getSharedData(){
        Object temp=sharedData;
        sharedData=null;
        return temp;
    }

}
