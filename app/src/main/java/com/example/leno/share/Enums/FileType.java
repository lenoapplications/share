package com.example.leno.share.Enums;

/**
 * Created by LeNo on 14.9.2017..
 */

public enum FileType {

    UNKNOWN,
    ROOT,DIRECTORY,
    MP3,MP4,
    JPG,PNG,JPEG,
    TXT,PDF
}
