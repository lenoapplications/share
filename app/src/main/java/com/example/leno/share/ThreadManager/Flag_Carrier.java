package com.example.leno.share.ThreadManager;



/**
 *
 * Internal imports
 *
 * */
import com.example.leno.share.Enums.ServerStatus;
import com.example.leno.share.Enums.ThreadManager_enums;




/**
 *
 * Java imports
 *
 * */




/**------------------------------------------------------------------------------------------------*/


/**
 * Created by LeNo on 5.9.2017..
 *
 *
 * Purpose of class:
 *          This class will be used like "Semaphore", it will hold enum flags of "ThreadManagerEnum",
 *          and "Server status enums",that why "ThreadManager_generic" can communicate with threads.
 *
 *
 *
 *          Section of class:
 *              First section:
 *                          -> first section of class is for "Searching for server" part.
 *                             It consists of enum and methods for changing status and getting
 *                             current status on thread.
 *
 */

public class Flag_Carrier {

    private ThreadManager_enums T_connectPC_flag;
    private ServerStatus T_checkAddr_flag;

    private ServerStatus S_connection_flag;

    private ThreadManager_enums T_communication_flag;
    private ServerStatus S_communicationRequest_flag;


    public Flag_Carrier()
    {
        T_connectPC_flag= ThreadManager_enums.T_CONNECTPC_NACTIVE;
        T_checkAddr_flag=ServerStatus.S_IPSEARCH_T_AVAILABLE;
        S_connection_flag=ServerStatus.S_CONNECTION_NCONNECTED;

        T_communication_flag=ThreadManager_enums.T_COMMUNICATION_NACTIVE;
        S_communicationRequest_flag=ServerStatus.S_COMMUNICATION_NREQUEST;


    }


    /**
     * First section ->"SEARCHING FOR SERVER"-----------------------THREAD FLAG----------------------------
     */

    public boolean T_connectPC_isActive(){ return T_connectPC_flag== ThreadManager_enums.T_CONNECTPC_ACTIVE; }

    public void T_connectPC_Activate(){T_connectPC_flag= ThreadManager_enums.T_CONNECTPC_ACTIVE;}

    public void T_connectPC_Deactivate(){ T_connectPC_flag= ThreadManager_enums.T_CONNECTPC_NACTIVE; }




    /**
     * First section ->"SEARCHING FOR SERVER"-------------SERVER FLAG---AND---THREADS ----------------------
     */


    public  boolean T_checkAddr_isAvailable(){return T_checkAddr_flag==ServerStatus.S_IPSEARCH_T_AVAILABLE;}

    public void T_checkAddr_nAvailable(){T_checkAddr_flag=ServerStatus.S_IPSEARCH_T_NAVAILABLE;}

    public  void T_checkAddr_Available(){T_checkAddr_flag=ServerStatus.S_IPSEARCH_T_AVAILABLE;}



    /**
     *
     * Second section ->"CONNECTION STATUS" ----------------------------------------------------------------------
     *
     * */

    public boolean S_ifConnected(){return S_connection_flag==ServerStatus.S_CONNECTION_CONNECTED;}

    public void S_Connect(){S_connection_flag=ServerStatus.S_CONNECTION_CONNECTED;}

    public void S_Disconnect(){S_connection_flag=ServerStatus.S_CONNECTION_NCONNECTED;}



    /**
     *
     * Third section ->"COMMUNICATION" -------------------------THREAD--------------------------------------------
     *
     * */
    public boolean T_Communication_isActive(){return T_communication_flag== ThreadManager_enums.T_COMMUNICATION_ACTIVE;}

    public void T_Communication_nActive(){T_communication_flag=ThreadManager_enums.T_COMMUNICATION_NACTIVE;}

    public void T_Communication_Active(){T_communication_flag=ThreadManager_enums.T_COMMUNICATION_ACTIVE;}

    public void T_Communication_Interrupt(){T_communication_flag=ThreadManager_enums.T_COMMUNICATION_INTERRUPT;}

    public boolean T_Communication_isInterrupt(){return T_communication_flag==ThreadManager_enums.T_COMMUNICATION_INTERRUPT;}


    /**
     *
     * Third section ->"COMMUNICATION"--------------------------REQUEST STATUS------------------------------------------
     *
     * */
    public void S_communicationRequest_addRequest(ServerStatus rq){S_communicationRequest_flag=rq;}

    public ServerStatus S_communicationRequest_getRequest(){return S_communicationRequest_flag;}

    public void S_communicationRequest_nRequest(){S_communicationRequest_flag=ServerStatus.S_COMMUNICATION_NREQUEST;}

    public boolean S_communicationRequest_checkStartStop()
    {
        return S_communicationRequest_flag==ServerStatus.S_COMMUNICATION_START || S_communicationRequest_flag==ServerStatus.S_COMMUNICATION_STOP;
    }

}
