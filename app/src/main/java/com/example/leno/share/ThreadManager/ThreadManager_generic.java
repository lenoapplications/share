package com.example.leno.share.ThreadManager;
import android.util.Log;

/**
* Threads internal imports
* */
import com.example.leno.share.ErrorManager.ErrorManager_generic;
import com.example.leno.share.UI_Manager.UI_handler.UI_main;


/**
 *
 * Threads java imports
 *
 * */
import java.lang.reflect.Method;


/**----------------------------------------------------------------------------------------------------------------*/




/**
 * Created by LeNo on 5.9.2017..
 *
 * Purpose of class:
 *              -class for function that will manage all processes that needs to be run on another thread.
 *
 *              Detailed description:
 *
 *                          Basic: we pass method and his arguments and then we create thread that will run that method.
 *                          The way we control it is by flags
 *
 *                          "threadInit" is a entry function for creating thread.Before creating ,function "checkFreeThreads"
 *                          checks what type of thread we want to create and if it is available.If conditions are satisfied
 *                          then we call "startThread" to create/start thread.
 *
 *
 *
 *
 *
 */

public class ThreadManager_generic {

    private final int T_CONNECTING_PC =0;
    private final int T_SEARCHPC_CHECKADDR=1;
    private final int T_COMMUNICATION=2;
    private final int FREE=-1;

    private final Flag_Carrier flgCarr;


    public ThreadManager_generic(Flag_Carrier flgC){

        flgCarr=flgC;
    }

    public <A,B,C,D> boolean thread_Init(final Object methodObj,
                                       final String methodName,
                                       final ErrorManager_generic err,
                                       final ThreadManager_dataHolder dtHld,
                                       final UI_main UI_h,
                                       final A objA,
                                       final B objB,
                                       final C objC,
                                       final D objD,
                                       Class<A> a,
                                       Class<B>b,
                                       Class<C>c,
                                       Class<D> d,
                                       int threadID)
    {
        try
        {
            if (checkFreeThreads(threadID))
            {
                final Method mth = methodObj.getClass().getMethod
                        (methodName, Flag_Carrier.class,ThreadManager_dataHolder.class,UI_main.class ,ErrorManager_generic.class, a, b,c,d);
                startThread(methodObj, mth, flgCarr,dtHld,UI_h ,err, objA, objB,objC,objD);
                return true;
            }
        }
        catch (NoSuchMethodException e)
        {
            e.printStackTrace();
            return false;
        }
        return false;
    }


    public <A,B,C> boolean thread_Init(final Object methodObj,
                                       final String methodName,
                                       final ErrorManager_generic err,
                                       final ThreadManager_dataHolder dtHld,
                                       final UI_main UI_h,
                                       final A objA,
                                       final B objB,
                                       final C objC,
                                       Class<A> a,
                                       Class<B>b,
                                       Class<C>c,
                                       int threadID)
    {
        try
        {
            if (checkFreeThreads(threadID))
            {
                final Method mth = methodObj.getClass().getMethod
                        (methodName, Flag_Carrier.class,ThreadManager_dataHolder.class,UI_main.class ,ErrorManager_generic.class, a, b,c);
                startThread(methodObj, mth, flgCarr,dtHld,UI_h ,err, objA, objB,objC);
                return true;
            }
        }
        catch (NoSuchMethodException e)
        {
            e.printStackTrace();
            return false;
        }
        return false;
    }



    public <A,B> boolean thread_Init(final Object methodObj,
                                         final String methodName,
                                         final ErrorManager_generic err,
                                         final ThreadManager_dataHolder dtHld,
                                         final UI_main Ui_h,
                                         final A objA,
                                         final B objB,
                                         Class<A> a,
                                         Class<B>b,
                                         int threadID)
    {
        try
        {
            if (checkFreeThreads(threadID))
            {
                final Method mth = methodObj.getClass().getMethod
                        (methodName, Flag_Carrier.class,ThreadManager_dataHolder.class,UI_main.class ,ErrorManager_generic.class, a, b);
                startThread(methodObj, mth, flgCarr,dtHld,Ui_h ,err, objA, objB);
                return true;
            }
        }
        catch (NoSuchMethodException e)
        {
            e.printStackTrace();
            return false;
        }
        return false;
    }


    public <A> boolean thread_Init(final Object methodObj,
                                    final String methodName,
                                    final ErrorManager_generic err,
                                    final ThreadManager_dataHolder dtHld,
                                    final UI_main UI_h,
                                    final A objA,
                                    Class<A> a,
                                    int threadID)

    {
        try
        {
            if (checkFreeThreads(threadID))
            {
                final Method mth = methodObj.getClass().getMethod
                        (methodName, Flag_Carrier.class,ThreadManager_dataHolder.class,UI_main.class ,ErrorManager_generic.class, a);
                startThread(methodObj, mth, flgCarr,dtHld,UI_h ,err, objA);
                return true;
            }
        }
        catch (NoSuchMethodException e)
        {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    public <A> boolean thread_Init(final Object methodObj,
                                   final String methodName,
                                   final A objA,
                                   Class<A> a,
                                   int threadID)

    {
        try
        {
            if (checkFreeThreads(threadID))
            {
                final Method mth = methodObj.getClass().getMethod(methodName,a);
                startThread(methodObj, mth,objA);
                return true;
            }
        }
        catch (NoSuchMethodException e)
        {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    public <A,B> boolean thread_Init(final Object methodObj,
                                   final String methodName,
                                   final A objA,
                                   final B objB,
                                   Class<A> a,
                                   Class<B> b,
                                   int threadID)

    {
        try
        {
            if (checkFreeThreads(threadID))
            {
                final Method mth = methodObj.getClass().getMethod(methodName,a,b);
                startThread(methodObj, mth,objA,objB);
                return true;
            }
        }
        catch (NoSuchMethodException e)
        {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    public void thread_initNoArgs(final Object methodObj,String methodName){

        try
        {
            final Method mth = methodObj.getClass().getDeclaredMethod(methodName);
            startThread(methodObj, mth);
        }
        catch(NoSuchMethodException e)
        {
            e.printStackTrace();
        }
    }



    private boolean checkFreeThreads(int threadID){

        switch(threadID)
        {
            case FREE:
            {
                return true;
            }
            case T_CONNECTING_PC:
            {
                if (!flgCarr.T_connectPC_isActive())
                {
                    flgCarr.T_connectPC_Activate();
                    return true;
                }
                return false;
            }


            case T_SEARCHPC_CHECKADDR:
            {
                return flgCarr.T_checkAddr_isAvailable();
            }


            case T_COMMUNICATION:
            {
                if (!flgCarr.T_Communication_isActive())
                {
                    flgCarr.T_Communication_Active();
                    return true;
                }
                Log.d("ROOT","FALSE");
                return false;
            }

        }
        return false;
    }



    private void startThread(final Object methodObj,final Method mth,final Object...args){

        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    mth.invoke(methodObj,args);
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
