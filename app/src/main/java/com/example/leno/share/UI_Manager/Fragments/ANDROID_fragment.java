package com.example.leno.share.UI_Manager.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.leno.share.Communication.Client;
import com.example.leno.share.Enums.ServerStatus;
import com.example.leno.share.ErrorManager.ErrorManager_generic;
import com.example.leno.share.FileManager.ANDROID.Data_android;
import com.example.leno.share.FileManager.ANDROID.FileManager_android;
import com.example.leno.share.FileManager.FileManager;
import com.example.leno.share.FileManager.PC.FileManager_pc;
import com.example.leno.share.R;
import com.example.leno.share.ThreadManager.Flag_Carrier;
import com.example.leno.share.ThreadManager.ThreadManager_dataHolder;
import com.example.leno.share.ThreadManager.ThreadManager_generic;
import com.example.leno.share.UI_Manager.UI_handler.UI_main;

import java.io.File;

/**
 * Created by LeNo on 25.9.2017..
 */

public class ANDROID_fragment extends Fragment {

    private boolean CREATED=false;

    private ThreadManager_dataHolder dtHld;
    private ThreadManager_generic thdMng;

    private Flag_Carrier flgCarr;
    private UI_main UIhnd;
    private Client client;
    private ErrorManager_generic errMng;
    private FileManager_android flMngAnd;

    private GridAdapter_android adpt;

    public void objects_init(ThreadManager_dataHolder thD,
                             ThreadManager_generic thM,
                             Flag_Carrier flgC,
                             UI_main UIh,
                             Client cl,
                             ErrorManager_generic errM,
                             FileManager_android flMnAd)
    {
        dtHld=thD;
        thdMng=thM;
        flgCarr=flgC;
        UIhnd=UIh;
        client=cl;
        errMng=errM;
        flMngAnd=flMnAd;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_android, container, false);

        gridView_init(view);

        buttons_init(view);

        root_init();

        return view;

    }

    private void root_init()
    {
        if (!CREATED)
        {
            thdMng.thread_initNoArgs(flMngAnd, "roots_init");
            CREATED=true;
        }
        UIhnd.closeDrawer_left();
    }


    private void buttons_init(View view)
    {
        ImageButton selectAll=(ImageButton)view.findViewById(R.id.buttonImage_selectAll_android);
        ImageButton chooseDwnLc=(ImageButton) view.findViewById(R.id.buttonImage_chooseDwnFolder_android);
        ImageButton createFolder=(ImageButton) view.findViewById(R.id.buttonImage_createFolder_android);

        Button upload=(Button)view.findViewById(R.id.button_upload_android);

        selectAll.setOnClickListener(selectAll_onClickListener());
        chooseDwnLc.setOnClickListener(chooseDwnFolder_onClickListener());
        createFolder.setOnClickListener(createDwnFolder_onClickListener());

        upload.setOnClickListener(upload_onClickListener());
    }

    private View.OnClickListener selectAll_onClickListener()
    {
        return new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                flMngAnd.selectAll();
                adpt.notifyDataSetChanged();
            }
        };
    }

    private View.OnClickListener chooseDwnFolder_onClickListener()
    {
        return new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String folder=flMngAnd.changeDwnFolder();

                SharedPreferences.Editor edit=getActivity().getSharedPreferences("tempData",Context.MODE_PRIVATE).edit();

                edit.putString("tempDwnFolderPath",folder);

                edit.apply();

                UIhnd.Toast(R.string.Toast_DwnFolder_changed, Toast.LENGTH_SHORT,folder.substring(folder.lastIndexOf("/")));

            }
        };
    }

    private View.OnClickListener createDwnFolder_onClickListener()
    {
        return new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Data_android root;

                if ((root=flMngAnd.getRoot())!=null) flMngAnd.createDefaultDwnlFolder(new File(root.getAbsPath()));

                else flMngAnd.createDefaultDwnlFolder(Environment.getExternalStorageDirectory());

                flMngAnd.reloadeRoot();
            }
        };
    }


    private View.OnClickListener upload_onClickListener()
    {
        return new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                UIhnd.Loading_show(R.string.downloading, UIhnd, flgCarr, "0", "0");
                flgCarr.S_communicationRequest_addRequest(ServerStatus.S_COMMUNICATION_UPLOAD);
                thdMng.thread_Init(client, "sendRequest_init", flMngAnd, FileManager.class, 2);
            }
        };
    }




    class GridAdapter_android extends BaseAdapter {

        class DataHolder {
            TextView text;
            ImageView imageFileType;
            ImageView imageCheck;
        }

        private ANDROID_fragment.GridAdapter_android.DataHolder holder;
        private GridView gView;

        public GridAdapter_android(GridView gV)
        {
            gView=gV;
        }


        @Override
        public int getCount()
        {
            return flMngAnd.getSize();
        }

        @Override
        public Object getItem(int position)
        {
            return flMngAnd.files.get(position);
        }

        @Override
        public long getItemId(int position)
        {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {

            View row = convertView;

            if (row == null) row = row_init(parent);

            else holder = (ANDROID_fragment.GridAdapter_android.DataHolder) row.getTag();


            imageLoad(holder.imageFileType,position);

            if (flMngAnd.isFileSelected((Data_android)flMngAnd.files.get(position)))
            {
                holder.imageCheck.setImageResource(R.drawable.pngimage_checkmark);
            }
            else holder.imageCheck.setImageDrawable(null);

            return row;
        }


        private View row_init(ViewGroup parent)
        {
            View row =((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.fragment_row,parent,false);
            holder = new DataHolder();
            holder.text = (TextView) row.findViewById(R.id.text_fragmentRow);
            holder.imageFileType =(ImageView) row.findViewById(R.id.image_fragmentRow);
            holder.imageCheck=(ImageView) row.findViewById(R.id.image_fragmentRow_fileChoosed);

            row.setTag(holder);

            return row;
        }

        private void imageLoad(ImageView image,int position)
        {
            if (flMngAnd.files.size()<1)return;

            Data_android file=(Data_android) getItem(position);

            holder.text.setText(file.getName(1));

            scalingSize(holder);

            switch(file.type)
            {
                case DIRECTORY:image.setImageResource(R.drawable.pngimage_folder);break;
                case TXT:image.setImageResource(R.drawable.pngimage_txt);break;
                case MP3:image.setImageResource(R.drawable.pngimage_music);break;
                case MP4:image.setImageResource(R.drawable.pngimage_music);break;
                case JPEG:image.setImageResource(R.drawable.pngimage_pic);break;
                case JPG:image.setImageResource(R.drawable.pngimage_pic);break;
                case PNG:image.setImageResource(R.drawable.pngimage_pic);break;
                case PDF:image.setImageResource(R.drawable.pngimage_pdf);break;
            }
        }

        private void scalingSize(DataHolder holder)
        {
            UIhnd.sizeMng.setViewSize
                    (holder.imageFileType,(LinearLayout.LayoutParams)holder.imageFileType.getLayoutParams(),0.066f,0.066f,gView.getWidth(),gView.getHeight());

            UIhnd.sizeMng.setViewSize
                    (holder.imageCheck,(LinearLayout.LayoutParams)holder.imageCheck.getLayoutParams(),0.026f,0.026f,gView.getWidth(),gView.getHeight());

            UIhnd.sizeMng.setFontSize_text(0.056f,0.056f,gView.getWidth(),gView.getHeight(),holder.text);

        }


    }



    private GridView gridView_init(View view)
    {
        GridView gView=(GridView) view.findViewById(R.id.grid_fragment_android);
        adpt=new GridAdapter_android(gView);

        gView.setAdapter(adpt);
        gView.setHorizontalScrollBarEnabled(true);
        gView.setOnItemClickListener(onItemClickListener(adpt));
        gView.setOnItemLongClickListener(onItemLongClickListener());

        UIhnd.changeAdapter(adpt);
        UIhnd.changeFragment(this);

        return gView;
    }


    private AdapterView.OnItemLongClickListener onItemLongClickListener()
    {
        return new AdapterView.OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
            {
                Data_android data= (Data_android) flMngAnd.files.get(position);
                thdMng.thread_Init(data,"moveIndxs",UIhnd,UI_main.class,-1);
                return true;
            }
        };
    }


    private AdapterView.OnItemClickListener onItemClickListener(final GridAdapter_android adpt)
    {

        return new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Data_android data= (Data_android) flMngAnd.files.get(position);

                switch(data.type)
                {
                    case DIRECTORY: onItemClick_openFolder(data); break;
                    case ROOT: onItemClick_openFolder(data); break;
                    default:flMngAnd.fileSelected(data);adpt.notifyDataSetChanged();
                }
            }
        };
    }

    private void onItemClick_openFolder(Data_android data)
    {
        UIhnd.Loading_show(R.string.open_fld_rt,UIhnd,flgCarr);
        thdMng.thread_Init(flMngAnd,"openFolder",data,Data_android.class,-1);
    }

}
