package com.example.leno.share.UI_Manager.Loading;

/**
 * Internal imports
 * */
import com.example.leno.share.R;
import com.example.leno.share.ThreadManager.Flag_Carrier;
import com.example.leno.share.UI_Manager.UI_handler.UI_main;

/**
 * Android imports
 * */
import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;



/**
 * Created by LeNo on 18.9.2017..
 */

public class Loading_info extends Dialog {

    private final UI_main UIhnd;
    private final Flag_Carrier flgCarr;
    private final String message;

    private TextView loading;

    public Loading_info(Activity activity,UI_main UIh,Flag_Carrier flgC,String msg) {
        super(activity);
        UIhnd=UIh;
        message=msg;
        flgCarr=flgC;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.loading_info);
        setCancelable(false);

        scalingSize(((TextView)findViewById(R.id.text_loadingInfo)));

        loading=((TextView)findViewById(R.id.text_loadingInfo));

        loading.setText(message);
    }

    private void scalingSize(TextView text)
    {
        UIhnd.sizeMng.setWindowSize(this.getWindow(),0.8f,0.09f);

        UIhnd.sizeMng.setViewSize(text,(LinearLayout.LayoutParams)text.getLayoutParams(),0.5f,0.5f,getWindow().getAttributes().width,getWindow().getAttributes().height);
        UIhnd.sizeMng.setFontSize_text(0.15f,0.20f,text.getLayoutParams().width,text.getLayoutParams().height,text);
    }

    public void download_upload_UpdateView(String status)
    {
        loading.setText(status);
    }

    @Override
    public void onBackPressed() {

        flgCarr.T_Communication_Interrupt();
    }
}
