package com.example.leno.share;

/**
* Internal app imports
    -> Package Initialization
                    ->Initialization_systemSetup class (Class for creating "dialog", in wich user choose to what IP address to connect)
*
* */
import com.example.leno.share.Communication.Client;
import com.example.leno.share.Enums.ServerStatus;
import com.example.leno.share.FileManager.ANDROID.FileManager_android;
import com.example.leno.share.FileManager.FileManager;
import com.example.leno.share.FileManager.PC.FileManager_pc;
import com.example.leno.share.ThreadManager.Flag_Carrier;
import com.example.leno.share.ThreadManager.ThreadManager_dataHolder;
import com.example.leno.share.ThreadManager.ThreadManager_generic;
import com.example.leno.share.ErrorManager.ErrorManager_generic;
import com.example.leno.share.UI_Manager.Dialogs.Dialog_connection;
import com.example.leno.share.UI_Manager.Fragments.ANDROID_fragment;
import com.example.leno.share.UI_Manager.Fragments.PC_fragment;
import com.example.leno.share.UI_Manager.UI_handler.UI_main;


/**
 *
 * Android imports
 */
import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.File;


/**
 *
 * Java imports
 *
 * */



/**-------------------------------------------------------------------------------------------------------------------------------------*/







/**
 * Created by LeNo on 4.9.2017..
 *
 *
 * Main entry point for application
 *
 *
 *          Description->
 *                  1.display "Connection" dialog
 *                  2.connect to server
 *                  3.remove "Connection" dialog (if connected successfully)
 *                  4.main window is available
 *                              -> after that choices are:
 *                                                      1.Open PC fragment
 *                                                      2.Open ANDROID fragment
 *
 */

public class app extends AppCompatActivity {

    private int activeFragment;

    private ThreadManager_dataHolder dtHld;
    private ThreadManager_generic thMng;

    private Flag_Carrier flgCarr;
    private UI_main UIhnd;
    private Client client;
    private ErrorManager_generic errMng;
    private FileManager_pc flMngPc;
    private FileManager_android flMngAnd;


    private DrawerLayout drwly;
    private ActionBarDrawerToggle actDrwly;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        drawLayout_init();
        appObjects_init();
        dialogConnection_init();

        requestPermissions_init();
    }

    /**
     * Init_application functions
     *
     * */
    private void dialogConnection_init()
    {
        Dialog_connection dialog=new Dialog_connection(this,thMng,errMng,dtHld,UIhnd,flgCarr,client);
        dialog.show();
    }

    private void drawLayout_init()
    {
        drwly=(DrawerLayout) findViewById(R.id.drawerLayout_mainLayout);
        actDrwly=new ActionBarDrawerToggle(this,drwly,R.string.drwly_Open,R.string.drwly_Close);
        actDrwly.setDrawerIndicatorEnabled(true);

        drwly.addDrawerListener(actDrwly);
        actDrwly.syncState();

        NavigationView nav_menu=(NavigationView) findViewById(R.id.navView_mainLayout);

        nav_menu.setNavigationItemSelectedListener(nav_menu_OnItemSelectedListener(this));
    }

    private void appObjects_init()
    {
        client=new Client();
        flgCarr=new Flag_Carrier();
        dtHld=new ThreadManager_dataHolder();
        thMng=new ThreadManager_generic(flgCarr);
        UIhnd=new UI_main(this);
        errMng=new ErrorManager_generic(thMng,this);
        flMngPc=new FileManager_pc();
        flMngAnd=new FileManager_android(UIhnd,this);



        activeFragment=-1;
    }


    /**
     * Nav_view setup
     *
     * */

    private NavigationView.OnNavigationItemSelectedListener nav_menu_OnItemSelectedListener(final app appAct)
    {
        return new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id=item.getItemId();

                switch(id)
                {
                    case (R.id.menu_android):fragment_init(1,getSupportFragmentManager());break;

                    case (R.id.menu_pc):fragment_init(0,getSupportFragmentManager());break;
                }
                return true;
            }
        };
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return actDrwly.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }



    /**
     *
     * Fragments
     *
     * */


    private void fragment_init(int frg, FragmentManager fm)
    {
        switch(frg)
        {
            case 0:fragment_pc(fm);break;
            case 1:fragment_android(fm);break;
        }

    }
    private void fragment_android(FragmentManager fm)
    {
        activeFragment=1;

        if (fm.findFragmentByTag("ANDROID")==null)
        {
            ANDROID_fragment andFrg=new ANDROID_fragment();

            andFrg.objects_init(dtHld,thMng,flgCarr,UIhnd,client,errMng,flMngAnd);

            fragment_create("ANDROID",andFrg,fm);
        }
        else
        {
            fragment_load("ANDROID", fm);
        }


    }

    private void fragment_pc(FragmentManager fm)
    {
        activeFragment=0;

        if (fm.findFragmentByTag("PC")==null)
        {
            PC_fragment pcFrg=new PC_fragment();

            flMngPc.addDwnlFolder(flMngAnd.getDwnFolder());

            pcFrg.objects_init(dtHld,thMng,flgCarr,UIhnd,client,errMng,flMngPc);

            fragment_create("PC",pcFrg,fm);
        }
        else
        {
            flMngPc.addDwnlFolder(flMngAnd.getDwnFolder());
            fragment_load("PC", fm);
        }
    }


    private void fragment_create(String tag,Fragment frg,FragmentManager fm)
    {
        FragmentTransaction ft=fm.beginTransaction();

        ft.addToBackStack(null);

        ft.replace(R.id.frameLayout_fragmentContainer,frg,tag);

        ft.commit();
    }

    private void fragment_load(String tag,FragmentManager fm)
    {
        FragmentTransaction ft=fm.beginTransaction();

        ft.addToBackStack(null);

        ft.replace(R.id.frameLayout_fragmentContainer,fm.findFragmentByTag(tag));

        ft.commit();
    }










    public void startApp()
    {
        UIhnd.addDrawerLayout_left(drwly);
        saveIpAddress_prefs();
    }


    @Override
    public void onBackPressed() {

        if (activeFragment==0)
        {
            UIhnd.Loading_show(R.string.close_fld_rt,UIhnd,flgCarr);
            flgCarr.S_communicationRequest_addRequest(ServerStatus.S_COMMUNICATION_CLOSE);
            thMng.thread_Init(client,"sendRequest_init",flMngPc,FileManager.class,2);
        }

        else if (activeFragment==1)
        {
            UIhnd.Loading_show(R.string.close_fld_rt,UIhnd,flgCarr);
            thMng.thread_initNoArgs(flMngAnd,"closeFolder");
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (!flgCarr.S_communicationRequest_checkStartStop())
        {
            flgCarr.S_communicationRequest_addRequest(ServerStatus.S_COMMUNICATION_STOP);
            thMng.thread_Init(client, "sendRequest_init", flMngPc, FileManager.class, 2);
        }
    }



    @Override
    protected void onRestart() {
        super.onRestart();

        if (!flgCarr.S_communicationRequest_checkStartStop())
        {
            flgCarr.S_communicationRequest_addRequest(ServerStatus.S_COMMUNICATION_START);
            thMng.thread_Init(client,"sendRequest_init",flMngPc,FileManager.class,2);
        }
    }


    /**
     * TEMP DATA-SAVE
     *
     * */

    private void saveIpAddress_prefs()
    {
        SharedPreferences.Editor edit=getSharedPreferences("tempData",MODE_PRIVATE).edit();
        edit.putString("tempIpAddress",client.ipAddressOfPC);
        edit.apply();
    }


    private void requestPermissions_init()
    {
       if (ContextCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED){
           ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},0);
       }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
