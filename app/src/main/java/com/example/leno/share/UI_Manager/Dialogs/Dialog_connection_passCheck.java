package com.example.leno.share.UI_Manager.Dialogs;

/**
 *
 * Internal imports
 *
 * */

import com.example.leno.share.R;
import com.example.leno.share.ThreadManager.Flag_Carrier;
import com.example.leno.share.ThreadManager.ThreadManager_dataHolder;
import com.example.leno.share.UI_Manager.Dialogs.Dialog_connection;
import com.example.leno.share.UI_Manager.UI_handler.UI_main;

/**
 *
 * Android imports
 *
 * */

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


/**
 * Created by LeNo on 8.9.2017..
 *
 * Purpose of class:
 *              when connection is established we need to check if user is allowed to connect to PC.
 *
 *              Detailed description:
 *                      When connection is established "Client" is sending request "6392" through "ThreadManager_dataHolder"
 *                      "Dialog_connection" is recv.that request and starts the function "DialogConn_switchDialog" in "UI_main" class
 *                      which will create this dialog.
 *
 *                      If user clicks on "next" button it will send response 6391 which will tell "Client" to continue the search for server.
 *                      After click, "while(true)" loop will start which will wait for requests from "Client" and there are only two possible request:
 *                      6392 and 6391.
 *
 *                      example:
 *                          """""
 *                              while(true)
 {
                                     if (dtHld.waitRequest(6392)) -> another connection is found
                                     {
                                     ((TextView) findViewById(R.id.text_initializationPassCheck_connect)).setText("Enter password :  ".concat((String)dtHld.getSharedData()));
                                     break;
                                     }
                                     else if (dtHld.waitRequest(6390)) -> client has finished the "search",kill this dialog
                                     {
                                     dialog_connection.show();
                                     dismiss();
                                     break;
                                     }
                                 }
                            """""
 *
 */

public class Dialog_connection_passCheck extends Dialog {

    private final Activity activity;
    private final Dialog_connection dialog_connection;
    private final ThreadManager_dataHolder dtHld;
    private final Flag_Carrier flgCarr;
    private final UI_main UIhnd;

    public Dialog_connection_passCheck(Activity act, Dialog_connection dc, ThreadManager_dataHolder dtH,Flag_Carrier flgC,UI_main UI_h){
        super(act);
        activity=act;
        dialog_connection=dc;
        dtHld=dtH;
        flgCarr=flgC;
        UIhnd=UI_h;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.initialization_dialog_connect_passcheck);
        setCancelable(false);

        initViews();

    }

    private void initViews()
    {
        Button connect=(Button) findViewById(R.id.button_initializationPassCheck_connect);
        Button next=(Button) findViewById(R.id.button_initializationPassCheck_next);

        scalingSize(connect,next);
        onClickListenerSetup(connect,next);

        setText_nameOfPc();
    }

    private void scalingSize(Button connect,Button next)
    {

        UIhnd.sizeMng.setWindowSize(this.getWindow(),0.9f,0.4f);

        UIhnd.sizeMng.setViewSize(connect,(ConstraintLayout.LayoutParams)connect.getLayoutParams(),0.4f,0.2f,this.getWindow().getAttributes().width,this.getWindow().getAttributes().height);
        UIhnd.sizeMng.setViewSize(next,(ConstraintLayout.LayoutParams)next.getLayoutParams(),0.4f,0.2f,this.getWindow().getAttributes().width,this.getWindow().getAttributes().height);
        UIhnd.sizeMng.setFontSize_button(0.2f,0.2f,connect.getLayoutParams().width,connect.getLayoutParams().height,connect,next);
    }



    private void setText_nameOfPc(){

        ((TextView) findViewById(R.id.text_initializationPassCheck_connect)).setText("Enter password : ".concat((String)dtHld.getSharedData()));
    }

    private void onClickListenerSetup(Button connect,Button next){

        connect.setOnClickListener(connectButton_onClickListener());
        next.setOnClickListener(nextButton_onClickListener());
    }


    private View.OnClickListener connectButton_onClickListener(){

        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText pass=(EditText) findViewById(R.id.edit_initializationPassCheck_password);

                if (pass.getText().toString().length()==0)
                {
                    Toast.makeText(activity,"Enter password",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    dtHld.sendResponse(6391,"connect :".concat(pass.getText().toString()));
                    dataHolder_requestLoop();
                }
            }
        };
    }

    private View.OnClickListener nextButton_onClickListener(){

        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dtHld.sendResponse(6391,"next");
                dataHolder_requestLoop();
            }
        };
    }


    private void dataHolder_requestLoop(){

        while(true)
        {
            if (dtHld.waitRequest(6392))
            {
                ((TextView) findViewById(R.id.text_initializationPassCheck_connect)).setText("Enter password :  ".concat((String)dtHld.getSharedData()));
                break;
            }
            else if (dtHld.waitRequest(6390))
            {
                dialog_connection.show();
                dismiss();
                break;
            }
        }
    }

}
