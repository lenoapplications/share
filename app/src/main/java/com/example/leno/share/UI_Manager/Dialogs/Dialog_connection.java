package com.example.leno.share.UI_Manager.Dialogs;


/**
 *
 * Internal imports
 *
 * */

import com.example.leno.share.Communication.Client;
import com.example.leno.share.ErrorManager.ErrorManager_generic;
import com.example.leno.share.R;
import com.example.leno.share.ThreadManager.Flag_Carrier;
import com.example.leno.share.ThreadManager.ThreadManager_dataHolder;
import com.example.leno.share.ThreadManager.ThreadManager_generic;
import com.example.leno.share.UI_Manager.UI_handler.UI_main;

/**
 *
 * Android imports
 *
 * */
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


/**
 * Created by LeNo on 6.9.2017..
 *
 *Purpose of class:
 *              class for creating small  "start" menu. Menu will have buttons for seaching,connecting
 *              and downloading application for PC.
 *
 *              Detailed description:
 *                      -> setting up window
 *                          with "SizeManager" object windows size is modified.Dialog is using 90% of complete width of phone
 *                          for width and 60% of height height of phone for height.
 *                          example("UIhnd.sizeMng.setWindowSize(this.getWindow(),0.9f,0.6f)");
 *
 *
 *
 *                      ->init buttons and define "clicks".
 *
 *                      -> search button "onClickListener" setup
 *
 *                          search button will start the " searchServer_Init" function in "Client" class.
 *                          It will start in "ThreadManager_generic", it will check that this process is
 *                          not already started, if process was not started, it will run the "searchServer_init".
 *                          Another thread we start is for the function "searchButton_passwordCheckDialog_init".
 *                          In this function we are waiting for a response "6392" ("ThreadManager_dataholder" class)
 *                          that will trigger the switch from this dialog to "Dialog_connection_passCheck"
 *                          Switch is happening in "UI_main" class, because we are not in main UI thread,so we
 *                          need to use "Mainlooper" in "UI_main" class.
 *                          
 *                          If response is "6390" ,break the loop and stop the thread (Means no connection was found)
 *
 *
 *
 *
 */

public class Dialog_connection extends Dialog {

    private final Activity activity;
    private final ThreadManager_generic thrMng;
    private final ErrorManager_generic errMng;
    private final ThreadManager_dataHolder dtHld;
    private final UI_main UIhnd;
    private final Flag_Carrier flgCarr;
    private final Client client;


    public Dialog_connection(Activity act,
                             ThreadManager_generic tMng,
                             ErrorManager_generic err,
                             ThreadManager_dataHolder dataHld,
                             UI_main UI_h,
                             Flag_Carrier flgC, Client cl){
        super(act);

        activity=act;
        thrMng=tMng;
        errMng=err;
        client=cl;
        dtHld=dataHld;
        UIhnd =UI_h;
        flgCarr=flgC;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.initialization_dialog_connect);
        setCancelable(false);

        initViews();

    }

    private void initViews()
    {
        Button connect=(Button)findViewById(R.id.button_initializationConnect);
        Button search=(Button) findViewById(R.id.button_initializationSearchPC);
        TextView ipAddress=(TextView) findViewById(R.id.text_initializationIPaddress);
        TextView password=(TextView) findViewById(R.id.text_initializationPassword);

        scalingSize(connect,search,ipAddress,password);
        onClickListenerSetup(connect,search);

        ((EditText)findViewById(R.id.edit_initializationIpAddress)).setText(loadLastIp());
    }

    private String loadLastIp()
    {
        SharedPreferences prefs=activity.getSharedPreferences("tempData", Context.MODE_PRIVATE);

        return prefs.getString("tempIpAddress",null);
    }

    private void scalingSize(Button connect,Button search,TextView ipAddress,TextView password)
    {
        UIhnd.sizeMng.setWindowSize(this.getWindow(),0.9f,0.6f);

        UIhnd.sizeMng.setViewSize(connect,(ConstraintLayout.LayoutParams)connect.getLayoutParams(),0.25f,0.135f,this.getWindow().getAttributes().width,this.getWindow().getAttributes().height);
        UIhnd.sizeMng.setViewSize(search,(ConstraintLayout.LayoutParams)search.getLayoutParams(),0.25f,0.15f,this.getWindow().getAttributes().width,this.getWindow().getAttributes().height);

        UIhnd.sizeMng.setViewSize(ipAddress,(ConstraintLayout.LayoutParams)ipAddress.getLayoutParams(),0.3f,0.05f,this.getWindow().getAttributes().width,this.getWindow().getAttributes().height);
        UIhnd.sizeMng.setViewSize(password,(ConstraintLayout.LayoutParams)password.getLayoutParams(),0.3f,0.05f,this.getWindow().getAttributes().width,this.getWindow().getAttributes().height);

        UIhnd.sizeMng.setFontSize_button(0.25f,0.2f,connect.getLayoutParams().width,connect.getLayoutParams().height,connect,search);
        UIhnd.sizeMng.setFontSize_text(0.25f,0.2f,ipAddress.getLayoutParams().width,ipAddress.getLayoutParams().height,ipAddress,password);
    }



    private void onClickListenerSetup(Button connect,Button search){

        connect.setOnClickListener(connectButton_onClickListener(this));
        search.setOnClickListener(searchButton_onClickListener(this));
    }


    private View.OnClickListener connectButton_onClickListener(final Dialog_connection thisDialog)
    {

        return new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String password=((TextView)findViewById(R.id.edit_initializationPassword)).getText().toString();
                String ipAddress=((TextView) findViewById(R.id.edit_initializationIpAddress)).getText().toString();

                if (password.length()<1 || ipAddress.length()<1)Toast.makeText(activity,"Server not found",Toast.LENGTH_SHORT).show();

                else
                {
                    thrMng.thread_Init(client,
                            "connectServer_init",
                            errMng,
                            dtHld,
                            UIhnd,thisDialog,
                            ipAddress,
                            password,
                            Dialog_connection.class,
                            String.class,
                            String.class,
                            0);
                    UIhnd.Loading_show(R.string.loading,UIhnd,flgCarr);
                }
            }
        };
    }


    private View.OnClickListener searchButton_onClickListener(final Dialog_connection thisDialog){

        return new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(thrMng.thread_Init(client,"searchServer_Init",errMng,dtHld, UIhnd,activity,thrMng,thisDialog,Activity.class,ThreadManager_generic.class,Dialog_connection.class,0))
                {
                    Toast.makeText(activity,"Please wait....",Toast.LENGTH_LONG).show();
                    thrMng.thread_initNoArgs(thisDialog,"searchButton_passwordCheckDialog_init");
                }
            }
        };
    }

    public void searchButton_passwordCheckDialog_init(){


        while(true){

            if (dtHld.waitRequest(6392))
            {
                UIhnd.DialogConn_switchDialog(this,dtHld,flgCarr);
                break;
            }
            else if (dtHld.waitRequest(6390))
            {
                break;
            }
        }

    }
}
