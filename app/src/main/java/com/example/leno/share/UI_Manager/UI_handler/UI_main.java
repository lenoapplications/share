package com.example.leno.share.UI_Manager.UI_handler;

/**
 *
 * Internal imports
 *
 * */
import com.example.leno.share.FileManager.FileManager;
import com.example.leno.share.FileManager.PC.FileManager_pc;
import com.example.leno.share.ThreadManager.Flag_Carrier;
import com.example.leno.share.ThreadManager.ThreadManager_dataHolder;
import com.example.leno.share.UI_Manager.Dialogs.Dialog_connection;
import com.example.leno.share.UI_Manager.Dialogs.Dialog_connection_passCheck;
import com.example.leno.share.UI_Manager.Loading.Loading_info;
import com.example.leno.share.R;
import com.example.leno.share.app;
/**
 *
 * Android imports
 * */
import android.app.Activity;
import android.app.Dialog;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by LeNo on 8.9.2017..
 *
 * Purpose of class:
 *          this class will be used for communication between threads and UI_main thread.
 *
 *          It will have ref. to activity that is currently active and Handler that will run
 *          instructions from thread to UI_main thread.
 *
 *          For example if we are on "searchingPC" thread,where we are searching for connection,
 *          and we want to popup small window (Like Toast) we need to run Toast on UI_main thread,
 *          we cant run it on "searchingPC" thread.For that we can use function from this class,
 *          which will use Handler.post function for talking to UI_main thread.
 *
 *
 *          Note:
 *              "changeActivity" function -> with this function we choose to what activity we want to ref. as UI_main thread.
 *
 *              "startApp" function -> this function is called from "Client" class when connection is established with PC,
 *              and this function must only be called this way,because we are casting the "activity" to "app", so we can call
 *              the function startApp from "app" class.
 *
 *
 *
 *
 *
 */

public class UI_main {

    public final SizeManager sizeMng;

    private  Handler UI_hnd;
    private  Activity activity;

    private Loading_info loading;
    private BaseAdapter adpt;
    private Fragment frg;
    private DrawerLayout drwly_left;

    public UI_main(Activity act){

        UI_hnd=new Handler(act.getMainLooper());
        activity=act;
        sizeMng=new SizeManager(act);
    }

    public void changeActivity(Activity act)
    {
        activity=act;
        UI_hnd=new Handler(act.getMainLooper());
    }

    public void changeAdapter(BaseAdapter adapter)
    {
        adpt=adapter;
    }

    public void changeFragment(Fragment fragment)
    {
        frg=fragment;
    }

    public void addDrawerLayout_left(DrawerLayout drawerLayout)
    {
        drwly_left=drawerLayout;
    }



    /**
     *
     * VIEW functions
     *
     * */

    public void closeDrawer_left()
    {
        UI_hnd.post(new Runnable()
        {
            @Override
            public void run()
            {
                drwly_left.closeDrawers();
            }
        });
    }

    /**
     *
     * "DISPLAY INFO" functions
     *
     * */

    public void Toast(final int text, final int duration,final String...args)
    {
        UI_hnd.post(new Runnable()
        {
            @Override
            public void run()
            {
                Toast.makeText(activity,activity.getString(text,args),duration).show();
            }
        });
    }

    public void Loading_show(final int message,final UI_main th,final Flag_Carrier flgC,final String...args)
    {

        UI_hnd.post(new Runnable()
        {
            @Override
            public void run()
            {
                loading=new Loading_info(activity,th,flgC,activity.getString(message,args));
                loading.show();
            }
        });
    }

    public void Loading_downloadUpdate(final Integer count,final Integer downloaded)
    {
        UI_hnd.post(new Runnable()
        {
            @Override
            public void run()
            {
                loading.download_upload_UpdateView(activity.getString(R.string.downloading,downloaded.toString(),count.toString()));
            }
        });
    }

    public void Loading_uploadingUpdate(final Integer count,final Integer uploaded)
    {
        UI_hnd.post(new Runnable()
        {
            @Override
            public void run()
            {
                loading.download_upload_UpdateView(activity.getString(R.string.uploading,uploaded.toString(),count.toString()));
            }
        });
    }

    public void Loading_dismiss()
    {
        UI_hnd.post(new Runnable()
        {
            @Override
            public void run()
            {
                loading.dismiss();
            }
        });
    }

    /**
     *
     * "CONNECTING TO PC" functions
     *
     * */

    public void DialogConn_switchDialog(final Dialog_connection dc, final ThreadManager_dataHolder dtH, final Flag_Carrier flgCarr)
    {
        final UI_main temp=this;
        UI_hnd.post(new Runnable() {
            @Override
            public void run() {
                Dialog_connection_passCheck passCheck=new Dialog_connection_passCheck(activity,dc,dtH,flgCarr,temp);
                dc.hide();
                passCheck.show();
            }
        });
    }

    public void startApp(final Dialog_connection dialogConn)
    {
        UI_hnd.post(new Runnable()
        {
            @Override
            public void run()
            {
                dialogConn.dismiss();
                ((app)activity).startApp();
            }
        });

    }



    /**
     *
     * FRAGMENTS functions
     *
     * */


    public void notifyChange()
    {
        UI_hnd.post(new Runnable()
        {
            @Override
            public void run()
            {
                adpt.notifyDataSetChanged();
            }
        });
    }

    public void changeURL(final FileManager flMng, final String name, final int textID)
    {
        UI_hnd.post(new Runnable()
        {
            @Override
            public void run()
            {

                flMng.addToPath(name);

                ((TextView) activity.findViewById(textID)).setText(name);
            }
        });
    }

    public void changeURL(final FileManager flMng, final int textID)
    {
        UI_hnd.post(new Runnable()
        {
            @Override
            public void run()
            {
                ((TextView) activity.findViewById(textID)).setText(flMng.removeFromPath());
            }
        });
    }


    public void destroyFragment(int msg)
    {
        UI_hnd.post(new Runnable()
        {
            @Override
            public void run()
            {
                frg.getFragmentManager().beginTransaction().remove(frg).commit();
            }
        });
        Toast(msg,Toast.LENGTH_SHORT);
    }

    public void destroyFragment()
    {
        UI_hnd.post(new Runnable()
        {
            @Override
            public void run()
            {
                frg.getFragmentManager().beginTransaction().remove(frg).commit();
            }
        });
    }



    /***General purpose functions***/

    public void sleep(int mseconds)
    {
        try
        {
            Thread.sleep(mseconds);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }
}
