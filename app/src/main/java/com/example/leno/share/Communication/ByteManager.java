package com.example.leno.share.Communication;



/**
 *
 * Internal Imports
 * */
import android.util.Log;

import com.example.leno.share.FileManager.ANDROID.Data_android;
import com.example.leno.share.FileManager.ANDROID.FileManager_android;
import com.example.leno.share.ThreadManager.Flag_Carrier;
import com.example.leno.share.UI_Manager.UI_handler.UI_main;

/**
 * Java imports
 * */
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.nio.Buffer;
import java.util.Arrays;



/**
 *
 * Android imports
 * */


/**
 * Created by LeNo on 15.9.2017..
 *
 * Purpose of class:
 *          -> class for handling received bytes from input stream
 *
 *          Description->
 *                  recvResp function ->
 *                              -in while loop we wait for "fillResp" function return true,which means we have recivied
 *                               response to request.
 *                                  -"fillResp" function checks if there is some data in stream and if there is it reads it
 *                                   to bytes,which will then be added to "builder" (builder.append(new String(bytes)).
 *                                   If string contains '*' then that means all data has been downloaded,and can return true.
 *                 getResponse function ->
 *                                  function that is called after "recvResp" function return true.It returns string(request) from "builder" object
 *
 *                 clean functions ->
 *                          if there was some interruption when data was being downloaded from stream,on next request "clean" function
 *                          is called to clean previous data that was not fully downloaded.
 */

public class ByteManager{

    private final byte[] bytes;
    private final StringBuilder builder;
    private final Flag_Carrier flgCarr;


    private boolean stuck=false;

    public ByteManager(Flag_Carrier flgC)
    {
        bytes=new byte[1024];
        builder=new StringBuilder();
        flgCarr=flgC;
    }


    public boolean recvResp(BufferedInputStream inStream, String request, String position,long timeout) throws IOException
    {
        long time=System.currentTimeMillis();

        while(true)
        {
            if(flgCarr.T_Communication_isInterrupt())
            {
                cleanFull();
                throw new IOException("Error".concat(request).concat(":interrupted"));
            }
            if (fillResp(inStream,request,position))
            {
                Arrays.fill(bytes,(byte)0);
                return true;
            }
            if ((System.currentTimeMillis()-time)>timeout) throw new IOException("Error".concat(request).concat(":timeout"));
        }

    }

    private boolean fillResp(BufferedInputStream inStream, String request, String position)throws IOException
    {
        try
        {
            int bytesRead=inStream.read(bytes);

            if (bytesRead==-1)
            {
                cleanFull();
                throw new IOException("Error".concat(request).concat(":server closed:").concat(position));
            }
            builder.append(new String(bytes,0,bytesRead));

            return builder.charAt(builder.length()-1)=='*';
        }
        catch (SocketTimeoutException e)
        {
            return false;
        }
    }



    public void recvFiles(BufferedInputStream inStream, BufferedOutputStream outStream, File location, UI_main UIhnd,int count)throws IOException
    {
        try
        {
            int downloaded=0;

            while (true)
            {
                if (flgCarr.T_Communication_isInterrupt())
                {
                    outStream.close();
                    cleanFull();
                    throw new IOException("Error:download operation brokedown");
                }

                if (fillDownloadInfo(inStream))
                {
                    if (builder.toString().equals("COMPLETE*"))
                    {
                        cleanFull();
                        break;
                    }
                    else
                    {
                        UIhnd.Loading_downloadUpdate(count,downloaded);

                        if (!recvFile(Integer.parseInt(builder.substring(0,builder.indexOf(":"))),
                                createNewFile(location,builder.substring(builder.indexOf(":")+1,builder.length()-1)),
                                inStream,outStream))  break;

                        outStream.write("->*".getBytes());
                        outStream.flush();
                        cleanBuilder();
                        ++downloaded;
                    }
                }
            }
        }
        catch (IOException e)
        {
            outStream.close();
            cleanFull();
            throw new IOException("Error:download operation brokedown");
        }
    }

    private boolean recvFile(long size,File location,BufferedInputStream inputStream,BufferedOutputStream outStream)throws IOException
    {
        int bytesRead;

        FileOutputStream writeLocation=new FileOutputStream(location);

        while(size!=0)
        {
            if (flgCarr.T_Communication_isInterrupt())
            {
                outStream.close();
                writeLocation.close();
                cleanFull();
                throw new IOException("Error:download operation brokedown");
            }


            if ((bytesRead=fillDownloadFile(inputStream))!=-1)
            {
                writeLocation.write(bytes,0,bytesRead);
                writeLocation.flush();
                size-=bytesRead;
            }
        }
        writeLocation.close();
        return true;
    }


    private int fillDownloadFile(BufferedInputStream inStream) throws IOException
    {
        try{
            int bytesRead=inStream.read(bytes);

            if (bytesRead==-1)
            {
                cleanFull();
            }
            return bytesRead;
        }
        catch (SocketTimeoutException e)
        {
            return -1;
        }
    }

    private boolean fillDownloadInfo(BufferedInputStream inStream)throws IOException
    {
        try
        {
            int bytesRead=inStream.read(bytes);

            if (bytesRead==-1)
            {
                cleanFull();
                throw new IOException("Error-1:server closed:-1");
            }
            builder.append(new String(bytes,0,bytesRead));

            return builder.charAt(builder.length()-1)=='*';
        }
        catch (SocketTimeoutException e)
        {
            return false;
        }
    }


    private File createNewFile(File dir,String filename)
    {
        File file=new File(dir,filename);

        int number=(int)dir.length();

        while(file.exists())
        {
            file=new File(dir,Integer.toString(number).concat(filename));
            ++number;
        }
        return file;
    }





    public void sendFiles(BufferedInputStream inStream, BufferedOutputStream outStream, FileManager_android flMng,UI_main UIhnd) throws IOException
    {
        try
        {
            byte[] uplByt=new byte[10000];
            int count=flMng.getCart().size();
            int uploaded=0;

            recvResp(inStream,"7","-1",10000);

            for (Data_android data:flMng.getCart())
            {
                UIhnd.Loading_uploadingUpdate(count,uploaded);

                FileInputStream reader=setupFile(data.getAbsPath(),outStream);
                int bytesRead;

                while((bytesRead=reader.read(uplByt))!=-1)
                {
                    outStream.write(uplByt,0,bytesRead);
                    outStream.flush();
                }
                reader.close();

                recvResp(inStream,"7","-1",10000);

                ++uploaded;
            }
            outStream.write("COMPLETE*".getBytes());
            outStream.flush();
            cleanFull();
        }
        catch (IOException e)
        {
            cleanFull();
            outStream.close();
            throw new IOException("Error:upload operation brokedown");
        }
    }

    private FileInputStream setupFile(String absPath,BufferedOutputStream outStream) throws IOException
    {
        FileInputStream stream=new FileInputStream(new File(absPath));

        String info=Integer.toString(stream.available()).concat(":").concat(absPath.substring(absPath.lastIndexOf("/")+1)).concat("*");

        outStream.write(info.getBytes());

        outStream.flush();

        return stream;
    }



    /**
     *
     * STRINGBUILDER functions
     *
     * */
    public String getResponse()
    {
        return builder.toString();
    }

    /**
     *
     * CLEAN
     *
     *
     * */



    public void cleanBuilder()
    {
        builder.setLength(0);
    }

    public void cleanBytes()
    {
        Arrays.fill(bytes,(byte)0);
    }

    public void cleanFull()
    {
        builder.setLength(0);
        Arrays.fill(bytes,(byte)0);
    }


    public void cleanPipe(BufferedInputStream inStream) throws IOException {

        if (stuck)
        {
            while (inStream.available() > 0)
            {
                inStream.read(bytes);
            }
            Arrays.fill(bytes, (byte) 0);

            stuck=false;
        }
    }

    public void pipeStuck()
    {
        stuck=true;
    }
}
