package com.example.leno.share.FileManager;

import android.util.Log;

import com.example.leno.share.Enums.FileType;
import com.example.leno.share.FileManager.ANDROID.Data_android;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

/**
 * Created by LeNo on 14.9.2017..
 */

public class FileCart extends HashSet<Data_android> {



    public boolean onClickFile(Data_android o)
    {
        for (Data_android data: this)
        {
            if (data.getAbsPath().equals(o.getAbsPath()))
            {
                this.remove(data);
                return false;
            }
        }
        super.add(o);
        return true;
    }


    public void selectAll(ArrayList<Data> files)
    {
        for (Data data:files)
        {
            if (((Data_android)data).type== FileType.DIRECTORY || this.contains((Data_android)data)) continue;

            super.add((Data_android)data);
        }
    }


    public boolean contains(Data_android o)
    {
        for (Data_android data: this)
        {
            if (data.getAbsPath().equals(o.getAbsPath()))return true;
        }
        return false;
    }
}
