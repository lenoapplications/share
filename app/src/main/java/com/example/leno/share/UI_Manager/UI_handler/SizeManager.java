package com.example.leno.share.UI_Manager.UI_handler;





/**
 *
 * Android imports
 *
 * */
import android.app.Activity;
import android.support.constraint.ConstraintLayout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


/**
 * Created by LeNo on 12.9.2017..
 *
 * Purpose of class:
 *          ->adjusting size of views,windows,fonts....
 *          ->adjusting using "percentages"
 *
 *
 *          note:
 *              -> for setting font size,function is using pythagoras formula (a**2+b**2=c**2
 */

public class SizeManager {

    private  int width;
    private  int height;
    private  int orientation;
    private  float scaledDensity;
    private  float dpi;
    private  float xdpi;
    private  float ydpi;


    public SizeManager(Activity mainActivity)
    {
        DisplayMetrics dm=mainActivity.getResources().getDisplayMetrics();
        width=dm.widthPixels;
        height=dm.heightPixels;
        orientation=mainActivity.getResources().getConfiguration().orientation;
        scaledDensity=dm.scaledDensity;
        dpi=dm.densityDpi;
        xdpi=dm.xdpi;
        ydpi=dm.ydpi;
    }


    public <A extends Window> void setWindowSize(A window,float wPercent,float hPercent)
    {

        if (orientation==1) window.setLayout((int)(width*wPercent),(int)(height*hPercent));

        else window.setLayout((int)(height*wPercent),(int)(width*hPercent));


    }


    public <A extends View> void setViewSize(A view,ConstraintLayout.LayoutParams params,float wPercent,float hPercent,int parentWidth,int parentHeight)
    {

        params.width=Math.round(parentWidth*wPercent);
        params.height=Math.round(parentHeight*hPercent);
        view.setLayoutParams(params);
    }

    public <A extends View> void setViewSize(A view, LinearLayout.LayoutParams params, float wPercent, float hPercent, int parentWidth, int parentHeight)
    {
        params.width=Math.round(parentWidth*wPercent);
        params.height=Math.round(parentHeight*hPercent);

        view.setLayoutParams(params);
    }



    public void setFontSize_button(float wPercent,float hPercent,int parentWidth,int parentHeight,Button...buttons)
    {
        float fontPx=fontPxSize(parentWidth,parentHeight,wPercent,hPercent);

        for (Button button:buttons)
        {
            button.setTextSize(TypedValue.COMPLEX_UNIT_PX,fontPx);
        }
    }


    public void setFontSize_text(float wPercent, float hPercent, int parentWidth, int parentHeight, TextView...texts)
    {
        float fontPx=fontPxSize(parentWidth,parentHeight,wPercent,hPercent);

        for (TextView text:texts)
        {
            text.setTextSize(TypedValue.COMPLEX_UNIT_PX,fontPx);
        }
    }

    private float fontPxSize(float parentWidth,float parentHeight,float wPercent,float hPercent)
    {

        float x_px=(float)Math.pow((parentWidth*wPercent),2);
        float y_px=(float)Math.pow((parentHeight*hPercent),2);

        return (float)Math.sqrt(x_px+y_px)/2;

    }
}
