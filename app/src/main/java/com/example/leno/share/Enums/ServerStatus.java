package com.example.leno.share.Enums;

/**
 * Created by LeNo on 5.9.2017..
 *
 *
 * Purpose of enum:
 *          Used for telling application is there some communication between server currently going on.
 *
 *          Example:
 *              When searching for server (PC) we are going to use S_IPSEARCH_ACTIVE, which will indicate
 *              that we are currently searching for server and we should wait for result.
 *
 *          NOTATIONS:
 *              T->THREAD,
 *              D->DATA,
 *              S->SERVER,
 *
 *
 */

public enum ServerStatus {


    /**
     *
     * SERVER->Status on "searching for IP address"
     *
     *
     *          S_IPSEARCH_T_AVAILABLE -> this flag tells "searchServer (client function) loop if thread is available for "searching PC"
     *          S_IPSEARCH_T_NAVAILABLE -> if two threads are already started then "seachServer" loop will wait for available thread
     *
     *
     *
     *
     * */
    S_IPSEARCH_T_AVAILABLE,
    S_IPSEARCH_T_NAVAILABLE,

    /**
     *
     * SERVER CONNECTION ->Status on "connection between mobile and PC"
     *
     *
     *                     CONNECTED->mobile connected to PC
     *                     NCONNECTED -> no connection between mobile and PC
     *
     * */


    S_CONNECTION_CONNECTED,
    S_CONNECTION_NCONNECTED,




    /**
     *
     * COMMUNICATION -> Status on requests and response between mobile and PC
     *
     *
     * */

    S_COMMUNICATION_ERROR_OVERFLOW,
    S_COMMUNICATION_STATUS_NSUCCESS,
    S_COMMUNICATION_NREQUEST,
    S_COMMUNICATION_START,
    S_COMMUNICATION_STOP,

    S_COMMUNICATION_CLICKFILE,
    S_COMMUNICATION_CLICKALL,
    S_COMMUNICATION_LOADROOT,
    S_COMMUNICATION_OPEN,
    S_COMMUNICATION_CLOSE,
    S_COMMUNICATION_CHOOSEFOLDER,
    S_COMMUNICATION_CREATEFOLDER,

    S_COMMUNICATION_UPLOAD,
    S_COMMUNICATION_DOWNLOAD;

}
