Class name:"ErrorManager/ErrorManager_generic.java"

Created:5.9.2017. by LeNo






																	ErrorManager_generic documentatin
																	
																	
																	
																	
Short description:

Purpose of class is to "catch" all errors in one place and to fix the given error.
Class is divide in sections.One of the section is the "Error_connectingPC",that
handles error that occur when app connects to PC for the first time.
Another section is "Error_communicationPC",in which we handle errors that occur 
when we send/recive requests/response.






																ErrorManager_connectingPC
-still need work



																ErrorManager_communicationPC
																

Structure:
		
		function (line 86) "error_communication"->parameters(String msg, Object...args):
								
				This function takes msg (error message) that helps to identify what kind of error occured.
				Second parameters are objects that are needed for handling errors.
				Those objects are args[0]="Client",
								  args[1]="UI_main",
								  args[2]="Flag Carrier",
								  args[3]="BufferdInputStream",
								  args[4]="FileManagerPC",
								  args[5]="ThreadManager_dataHolder"
				
				
				Identifying message part:
								
								1.When server did not respond in 5 second."ByteManager" throws IOException with message "Error\\d+:timeout"
								
								2.When user press "back" button "Loading dialog" class will change "T_communication_flag" variable in 
								  "Flag Carrier" class which will be caught in "ByteManager" and it will throw IOException with message
								  "Error\\d+:interrupted".
								  
								3.When server closed connection,"ByteManager" throws IOException with message "Error\d+:server closed:(-1|\d+)"
								
								4.When app did not successfully sent "STOP" OR "START" request,"Client" class will throw IOException with message
								  "Error:stop request was not sent|Error:start request was not sent"
								  
								5.When connection was lost, but user still tries to send something,"Client" class will throw IOException with message
								  "Error:PIPE broken"
								  
		
		Functions for handling errors from above:
					
					"error_communication_responseTimeout"->parameters (Clinet,UI_main,BufferdOutputStream,int request) throws IOException:
						
						LINE 150-> client.pipeStuck():calls function "ByteManager.pipeStuck" to set "stuck" variable to true.When that variable is true
								   it will trigger a "ByteManager.cleanPipe" function to clean InputStream if some data was accidentlly sent but was not 
								   readed.
						
						LINE 152 ->switch loop:
										Important->request variable helps to identify what kind of request was app sending when error occured.
												  That why timeout message looks like "Error\\d(request)+:timeout".
										
										first case: 
											
											-if request is 0,that means error occured when "ROOT*" request was trying to be sent.
											To fix this error, function "UI_main.destroyFragment" is called.Because request "ROOT*" is
											send automatically when user clicks on "ANDROID" fragment,and if fragment is not destroyed 
											we would not initialized new fragment which will then send "ROOT*" request.Only when fragment is
											being reloaded or created for the first time is "ROOT*" request sent.
											
										
										
										second case:
											
											-if request is 1,means that error occured when users was trying to open folder/root,"OPEN" request.
											In that case application will informed server that there was an error, data was not recived.So server
											can close that folder that was open but application did not recived "new files".
										
										third case:
											
											-same as second we,only difference is that error occured when users was closing folder (going back one step)
											
											
										In all cases we call "Client.closingCommunication_complete" function to close "Loading dialog", and release threads
												


					"error_communication_interrupted->parameters(Client,UI_main,BufferdOutputStream,int request) throws IOException
						
						exactly the same as function before.Only difference is in first case,when "destroyFragment" is called.Here
						"R.string.Error_interrupt" is given as argument, to display that string in "TOAST"



					"error_communication_serverClosed->parameters(Client client,FileManagerPC,ThreadManager_dataHolder,request,position)":
					
						This function will be called every time when server closes a "Socket".
						When connection is close "ByteManager" throws IOException, something like a request to reconnect.
						
						LINE 219-> errCnt.nsuccess(1) (description of this function is in file "ErrorManager_counter")
						
						LINE 221-> if ("Client.reconnect()")
									calls function "reconnect" and simply connects to server again.
									If condition is true:
												1."Client.closingCommunication_thread()" -need to inform "ThreadManager_generic" 
																						  that this thread will be closed and another one can be open.
																						  
												2.if ("ErrorManager_counter.errorOverflow_communicationPC") -this condition is true if "recconection" to server
																											failed more than 2 times.
																											We call "error_communicationOverflow_CommunicationPC" function
												3.else 							
													-we send request that we were trying to sent before when error occured.
													 "thMng.thread_init("client","sendRequest_init",flMngPC,FileManagerPC.class,2)"
													 
													 
									else:
										-"Client.killSocket" function is called, to close socket 
										-"Client.closingCommunication_complete" function is also called
									
						
						
					

					"error_brokenPipe"(Client,UI_main,ThreadManager_dataHolder,FileManagerPC,request):
						
						-this function is handling error same as function above. Tries to reconnect,and send request again.
						 only difference if "reconnect" failed, fragment will be destroyed.
												"UI_main.destroyFragment('Connection crashed')"
						

					
					"error_communication_startStop(Client,FileManagerPC) throws IOException
						
						-this function is called when we are leaving the app to run in a background and "STOP" request was trying to be sent.
						 This is crucial,server needs to know if app is on stop,because that is the only way that server will know that application 
						 is destroyed. 
						 If we turn application back on,we sent "START" request, to server that means,that we did not destroy/exit
						 application and want to continue where we left,but if leave application "STOP" will be sent and that why
						 server nows that next time we tun application that this our "new connection" to server,we dont want to 
						 continue.
						 
						 
					"error_communicationOverflow_CommunicationPC(Client,Flag_Carrier,FileManagerPC)
						-sending server request "MEMORY LOST*"

